# Clear Skies
_Group Number 11-9_

| Team Member | EID | GitLab Username |
| ------ | ------ |  ------ |
| Moe Ghanem | mg63785 | @moeghanem |
| Risa Maida | rcm3532 | @risamaida2114 |
| Anshita Saini | as222688 | @anshitasaini |
| Daniel Sialm | dhs833 | @daniel.sialm |

**Project Leader:** Daniel Sialm

**Git SHA:** commit fca1f0210d5f2f86d3ecb3cf030985d1a09afdf6

**GitLab Pipelines:** https://gitlab.com/risamaida2114/cs373-idb/-/pipelines

**Website:** https://www.clear-skies.me/

| Team Member | Estimated Time to Completion | Actual Time to Completion |
| ------ | ------ |  ------ |
| Moe Ghanem | 10 | 22 |
| Risa Maida | 10 | 25 |
| Anshita Saini | 10 | 25 |
| Daniel Sialm | 10 | 25 |

**Comments:**
Pagination template inspired by last semester's UniverCity project, linked here: https://gitlab.com/coleweinman/swe-college-project

The dropdown collapse bar was creating using the following bootstrap documentation:
https://getbootstrap.com/docs/5.0/components/navbar/#responsive-behaviors
