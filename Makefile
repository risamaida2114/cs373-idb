install:
	cd front-end && npm install --legacy-peer-deps

ci:
	cd front-end && rm package-lock.json && rm -rf node_modules && npm install --legacy-peer-deps

start:
	cd front-end && npm start --legacy-peer-deps

test:
	cd front-end && npm test --legacy-peer-deps

vtest:
	cd front-end && npm test --legacy-peer-deps --detectOpenHandles