# flask routing for back-end

from flask import jsonify, request
from models import app, db, PlanetMoon, Satellite, Launch
from sqlalchemy import and_, or_, cast, Integer
from sqlalchemy.sql import func


@app.route("/search", methods=["GET"])
def site_search():
    sat_page = request.args.get("satpage", type=int)
    lnc_page = request.args.get("lncpage", type=int)
    plm_page = request.args.get("plmpage", type=int)
    per_page = request.args.get("numinst", type=int)
    search = request.args.get("search")

    if per_page is None:
        per_page = 12

    if search is not None:
        query_satellites = search_satellites(db.session.query(Satellite), search)
        query_launches = search_launches(db.session.query(Launch), search)
        query_planetsmoons = search_planetsmoons(db.session.query(PlanetMoon), search)

    sat_count = query_satellites.count()
    lnc_count = query_launches.count()
    plm_count = query_planetsmoons.count()

    if sat_page is not None:
        query_satellites = query_satellites.paginate(
            page=sat_page, per_page=per_page, error_out=False
        )
    if lnc_page is not None:
        query_launches = query_launches.paginate(
            page=lnc_page, per_page=per_page, error_out=False
        )
    if plm_page is not None:
        query_planetsmoons = query_planetsmoons.paginate(
            page=plm_page, per_page=per_page, error_out=False
        )

    satellites_output = [x.serialize() for x in query_satellites]
    launches_output = [x.serialize() for x in query_launches]
    planetsmoons_output = [x.serialize() for x in query_planetsmoons]

    metadata = {
        "total_satellites": sat_count,
        "satellites_on_page": len(satellites_output),
        "total_launches": lnc_count,
        "launches_on_page": len(launches_output),
        "total_planetsmoons": plm_count,
        "planetsmoons_on_page": len(planetsmoons_output),
    }

    return jsonify(
        {
            "satellites": satellites_output,
            "launches": launches_output,
            "planetsmoons": planetsmoons_output,
            "metadata": metadata,
        }
    )


def search_satellites(query, search):
    search_fields_in = [
        Satellite.name,
        Satellite.classification,
        Satellite.launch_date,
        Satellite.country,
        Satellite.type,
    ]
    search_fields_exact = [
        (Satellite.number, int),
        (Satellite.intldes, str),
        (Satellite.orbital_period, float),
    ]

    search = search.split()
    search_filters = []
    for term in search:
        for category in search_fields_in:
            search_filters.append(category.ilike("%" + term + "%"))
        for category, cast in search_fields_exact:
            try:
                search_filters.append(category == cast(term))
            except:
                pass
    return query.filter(or_(*search_filters))


@app.route("/satellites", methods=["GET"])
def get_satellites():
    page = request.args.get("page", type=int)
    per_page = request.args.get("numinst", type=int)
    sort = request.args.get("sort")
    desc = request.args.get("desc", type=bool)
    # filters
    size = request.args.get("size")  # categorical
    type = request.args.get("type")  # categorical
    country = request.args.get("country")  # categorical
    orbital_period = request.args.get("orbper")  # range indicated by '-'
    year = request.args.get("date")  # range indicated by '-'
    # searching
    search = request.args.get("search")

    query = db.session.query(Satellite)

    # filtering
    if size is not None:
        query = query.filter(Satellite.size == size.upper())
    if type is not None:
        query = query.filter(Satellite.type == type.upper())
    if country is not None:
        query = query.filter(Satellite.country == country.upper())
    if orbital_period is not None:
        range = orbital_period.split("-")
        query = query.filter(
            and_(
                Satellite.orbital_period >= float(range[0]),
                Satellite.orbital_period <= float(range[1]),
            )
        )
    if year is not None:
        range = year.split("-")
        query = query.filter(
            and_(
                cast(func.substr(Satellite.launch_date, 1, 4), Integer)
                >= float(range[0]),
                cast(func.substr(Satellite.launch_date, 1, 4), Integer)
                <= float(range[1]),
            )
        )

    # sorting
    if sort is not None and getattr(Satellite, sort.lower()) is not None:
        if desc:
            query = query.order_by(getattr(Satellite, sort.lower()).desc())
        else:
            query = query.order_by(getattr(Satellite, sort.lower()))

    # searching
    if search is not None:
        query = search_satellites(query, search)

    count = query.count()

    if per_page is None:
        per_page = 12

    if page is not None:
        query = query.paginate(page=page, per_page=per_page, error_out=False)

    # paginate
    output = [x.serialize() for x in query]

    metadata = {"total_satellites": count, "satellites_on_page": len(output)}

    return jsonify({"satellites": output, "metadata": metadata})


@app.route("/satellites/<int:sat_id>")
def get_satellite(sat_id):
    x = db.session.query(Satellite).filter(Satellite.number == sat_id).first()
    return jsonify({"satellite": x.serialize()})


def search_launches(query, search):
    search_fields_in = [
        Launch.name,
        Launch.date_utc,
        Launch.static_fire_date,
        Launch.details,
    ]
    search_fields_exact = [(Launch.flight_number, int), (Launch.ships, int)]

    search = search.split()
    search_filters = []
    for term in search:
        for category in search_fields_in:
            search_filters.append(category.ilike("%" + term + "%"))
        for category, cast in search_fields_exact:
            try:
                search_filters.append(category == cast(term))
            except:
                pass
    return query.filter(or_(*search_filters))


@app.route("/launches", methods=["GET"])
def get_launches():
    page = request.args.get("page", type=int)
    per_page = request.args.get("numinst", type=int)
    sort = request.args.get("sort")
    desc = request.args.get("desc", type=bool)
    # filters
    reused = request.args.get("reused")  # categorical
    success = request.args.get("success")  # categorical
    ships = request.args.get("ships", type=int)  # categorical
    year = request.args.get("date")  # range indicated by '-'
    static_year = request.args.get("sdate")  # range indicated by '-'
    # searching
    search = request.args.get("search")

    query = db.session.query(Launch)

    # filtering
    if reused is not None:
        query = query.filter(Launch.reused == reused.lower())
    if success is not None:
        query = query.filter(Launch.success == success.lower())
    if ships is not None:
        query = query.filter(Launch.ships == int(ships))
    if static_year is not None:
        range = static_year.split("-")
        query = query.filter(
            and_(
                cast(func.substr(Launch.static_fire_date, 1, 4), Integer)
                >= float(range[0]),
                cast(func.substr(Launch.static_fire_date, 1, 4), Integer)
                <= float(range[1]),
            )
        )
    if year is not None:
        range = year.split("-")
        query = query.filter(
            and_(
                cast(func.substr(Launch.date_utc, 1, 4), Integer) >= float(range[0]),
                cast(func.substr(Launch.date_utc, 1, 4), Integer) <= float(range[1]),
            )
        )

    # sorting
    if sort is not None and getattr(Launch, sort.lower()) is not None:
        if desc:
            query = query.order_by(getattr(Launch, sort.lower()).desc())
        else:
            query = query.order_by(getattr(Launch, sort.lower()))

    # searching
    if search is not None:
        query = search_launches(query, search)

    # paginate
    count = query.count()
    if per_page is None:
        per_page = 12

    if page is not None:
        query = query.paginate(page=page, per_page=per_page, error_out=False)

    output = [x.serialize() for x in query]

    metadata = {"total_launches": count, "launches_on_page": len(output)}

    return jsonify({"launches": output, "metadata": metadata})


@app.route("/launches/<int:lan_id>")
def get_launch(lan_id):
    x = db.session.query(Launch).filter(Launch.flight_number == lan_id).first()
    return jsonify({"launch": x.serialize()})


def search_planetsmoons(query, search):
    search_fields_in = [
        PlanetMoon.name,
        PlanetMoon.discovery_date,
        PlanetMoon.body_type,
        PlanetMoon.discoveredBy,
    ]
    search_fields_exact = [
        (PlanetMoon.id, int),
        (PlanetMoon.mass, float),
        (PlanetMoon.density, float),
        (PlanetMoon.gravity, float),
        (PlanetMoon.mean_radius, float),
        (PlanetMoon.avgTemp, int),
    ]

    search = search.split()
    search_filters = []
    for term in search:
        for category in search_fields_in:
            search_filters.append(category.ilike("%" + term + "%"))
        for category, cast in search_fields_exact:
            try:
                search_filters.append(category == cast(term))
            except:
                pass
    return query.filter(or_(*search_filters))


@app.route("/planetsmoons", methods=["GET"])
def get_planetsmoons():
    page = request.args.get("page", type=int)
    per_page = request.args.get("numinst", type=int)
    sort = request.args.get("sort")
    desc = request.args.get("desc", type=bool)
    # filters
    mass_exp = request.args.get("mass")  # range
    density = request.args.get("density")  # range
    gravity = request.args.get("gravity")  # range
    avg_temp = request.args.get("temp")  # range
    mean_radius = request.args.get("radius")  # range
    # searching
    search = request.args.get("search")

    query = db.session.query(PlanetMoon)

    # filtering
    if mass_exp is not None:
        mass_exp = [float(x) for x in mass_exp.split("-")]

        # Add a check to make sure that PlanetMoon.mass is not 0 before applying the filter
        mass_filter = or_(
            and_(or_(mass_exp[0] == 0, mass_exp[1] == 0), PlanetMoon.mass == 0),
            and_(
                func.log10(PlanetMoon.mass) >= mass_exp[0],
                func.log10(PlanetMoon.mass) <= mass_exp[1],
                PlanetMoon.mass != 0,
            ),
        )

        query = query.filter(mass_filter)
    if density is not None:
        density = [float(x) for x in density.split("-")]
        query = query.filter(
            and_(PlanetMoon.density >= density[0], PlanetMoon.density <= density[1])
        )
    if gravity is not None:
        gravity = [float(x) for x in gravity.split("-")]
        query = query.filter(
            and_(PlanetMoon.gravity >= gravity[0], PlanetMoon.gravity <= gravity[1])
        )
    if mean_radius is not None:
        mean_radius = [float(x) for x in mean_radius.split("-")]
        query = query.filter(
            and_(
                PlanetMoon.mean_radius >= mean_radius[0],
                PlanetMoon.mean_radius <= mean_radius[1],
            )
        )
    if avg_temp is not None:
        avg_temp = [float(x) for x in avg_temp.split("-")]
        query = query.filter(
            and_(PlanetMoon.avgTemp >= avg_temp[0], PlanetMoon.avgTemp <= avg_temp[1])
        )

    # sorting
    if sort is not None and getattr(PlanetMoon, sort.lower()) is not None:
        if desc:
            query = query.order_by(getattr(PlanetMoon, sort.lower()).desc())
        else:
            query = query.order_by(getattr(PlanetMoon, sort.lower()))

    # searching
    if search is not None:
        query = search_planetsmoons(query, search)

    # paginate
    count = query.count()

    if per_page is None:
        per_page = 12

    if page is not None:
        query = query.paginate(page=page, per_page=per_page, error_out=False)

    output = [x.serialize() for x in query]

    metadata = {"total_planetsmoons": count, "planetsmoons_on_page": len(output)}

    return jsonify({"planetsmoons": output, "metadata": metadata})


@app.route("/planetsmoons/<int:pm_id>")
def get_planetmoon(pm_id):
    x = db.session.query(PlanetMoon).filter(PlanetMoon.id == pm_id).first()
    return jsonify({"planetmoon": x.serialize()})


@app.route("/")
def hello_world():
    return "Clear Skies Backend"


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=True)
