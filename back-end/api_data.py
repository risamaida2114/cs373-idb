# pulls from APIs to create json files

import requests
import time
import json
from random import randrange


class SpaceData:
    def __init__(self):
        pass

    # https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles=deimos_(moon)
    def get_images(self, item):
        fileSource = item + ".json"
        f = open(fileSource)
        items = json.load(f)
        for i in items:
            # get name of item
            i_name = i["englishName"]
            # if moon, check with _(moon)
            if fileSource == "moons.json":
                url = (
                    "https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles="
                    + i_name
                    + "_(moon)"
                )
            else:
                url = (
                    "https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles="
                    + i_name
                    + "_(planet)"
                )
            wiki = requests.get(url)
            wiki_loaded = wiki.json()
            # print(wiki_loaded)
            w = wiki_loaded["query"]["pages"]
            for k in w.values():
                temp = k.get("original")
                if temp:
                    i["image"] = temp["source"]
                    print(temp["source"])
                else:
                    i["image"] = "./images/moon_def.jpg"
            # if default image on moon, recheck without _(moon)
            if i["image"] == "./images/moon_def.jpg":
                url = (
                    "https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles="
                    + i_name
                )
                wiki = requests.get(url)
                wiki_loaded = wiki.json()
                w = wiki_loaded["query"]["pages"]
                for k in w.values():
                    temp = k.get("original")
                    if temp:
                        i["image"] = temp["source"]

        with open(fileSource, "w") as outfile:
            outfile.write(json.dumps(items, indent=4))

    def get_satellite_info(self):
        url = "https://uphere-space1.p.rapidapi.com/satellite/list"

        headers = {
            "X-RapidAPI-Key": "eb5f972787msh37c64509c6f10e6p1a0fb7jsncfb1911af154",
            "X-RapidAPI-Host": "uphere-space1.p.rapidapi.com",
        }

        query = {"page": "1"}
        response = requests.request("GET", url, headers=headers, params=query)
        satellites = response.json()

        page = 2
        while len(satellites) < 500:
            time.sleep(2)
            query = {"page": str(page)}
            response = requests.request("GET", url, headers=headers, params=query)
            page += 1
            satellites.extend(response.json())

        # countries = data.get_satellite_country_names()

        # for s in satellites:
        #     print(s["country"])
        #     s["country"] = countries[s["country"]]

        with open("satellite.json", "w") as outfile:
            outfile.write(json.dumps(satellites, indent=4))

    def get_satellite_country_names(self):
        url = "https://uphere-space1.p.rapidapi.com/satellite/list/countries"

        headers = {
            "X-RapidAPI-Key": "eb5f972787msh37c64509c6f10e6p1a0fb7jsncfb1911af154",
            "X-RapidAPI-Host": "uphere-space1.p.rapidapi.com",
        }

        query = {"page": "1"}
        response = requests.request("GET", url, headers=headers, params=query)
        country_list = response.json()

        country_dict = dict()

        for item in country_list:
            print(country_list)
            country_dict[item["abbreviation"]] = item["name"]

        return country_dict

    def get_satellite_images(self):
        f = open("satellite.json")
        satellites = json.load(f)
        for s in satellites:
            satellite_num = s["number"]
            img_url = (
                "https://uphere-space.sfo2.digitaloceanspaces.com/images/satellites_numbers/"
                + satellite_num
                + ".webp"
            )
            response = requests.request("GET", img_url)
            if response.status_code != 200:
                img_url = "https://uphere-space.sfo2.digitaloceanspaces.com/images/satellites_numbers/default.webp"
                response = requests.request("GET", img_url)
            s["image"] = img_url
            print(img_url)
        with open("satellite.json", "w") as outfile:
            outfile.write(json.dumps(satellites, indent=4))

    def get_launches(self):
        launches_url = "https://api.spacexdata.com/v4/launches"
        launches = requests.get(launches_url).json()

        with open("launches.json", "w") as outfile:
            outfile.write(json.dumps(launches, indent=4))

    def get_planets(self):
        url = "https://api.le-systeme-solaire.net/rest/bodies/"
        data = requests.get(url).json()
        planets = []

        id = 1000
        for body in data["bodies"]:
            if body["isPlanet"] == True:
                body["id"] = id
                id += 1
                planets.append(body)

        with open("planets.json", "w") as outfile:
            outfile.write(json.dumps(planets, indent=4))

    def get_moons(self):
        url = "https://api.le-systeme-solaire.net/rest/bodies/"
        data = requests.get(url).json()
        moons = []

        id = 2000
        for body in data["bodies"]:
            if body["bodyType"] == "Moon":
                body["id"] = id
                id += 1
                moons.append(body)

        with open("moons.json", "w") as outfile:
            outfile.write(json.dumps(moons, indent=4))

    # gets month and date of satellite launch
    def append_date_month_satellites(self):
        f = open("satellite.json")
        items = json.load(f)
        for i in items:
            i["date"] = i["launch_date"][5:10]
            i["month"] = i["date"][0:2]
        with open("satellite.json", "w") as outfile:
            outfile.write(json.dumps(items, indent=4))

    # gets month and date of launch
    def append_date_month_launches(self):
        f = open("launches.json")
        items = json.load(f)
        for i in items:
            i["date"] = i["date_utc"][5:10]
            i["month"] = i["date"][0:2]
        with open("launches.json", "w") as outfile:
            outfile.write(json.dumps(items, indent=4))

    # sorts json by month -> for satellites and launches
    # not necessarily needed but increases efficiency
    # for getting 3 random per month
    def sort_by_month(self, list):
        fileSource = list + ".json"
        f = open(fileSource)
        items = json.load(f)
        items.sort(key=lambda x: x["month"])
        with open(fileSource, "w") as outfile:
            outfile.write(json.dumps(items, indent=4))

    # get instance of satellite by date for launch and
    # get 3 satellites by month randomly
    # first get all satellites of month
    def get_satellites_for_launch(self):
        launch_items = json.load(open("launches.json"))
        satellite_items = json.load(open("satellite.json"))
        # loop thru launches
        # get number for satellite that matches date if possible
        # get 3 random satellites
        for l in launch_items:
            month = l["month"]
            sat_list = []
            for s in satellite_items:
                if s["month"] == month:
                    if s["date"] == l["date"]:
                        l["s_by_date"] = s["number"]
                    else:
                        sat_list.append(s)
                # break from unnecessary months
                elif int(month) < int(s["month"]):
                    break
            s_size = len(sat_list)
            random1 = randrange(0, s_size)
            l["s_1"] = sat_list[random1]["number"]
            random2 = randrange(0, s_size)
            # in the very off chance that randoms are the same loop until not
            while random1 == random2:
                random2 = randrange(0, s_size)
            random3 = randrange(0, s_size)
            while random1 == random3 or random2 == random3:
                random3 = randrange(0, s_size)
            l["s_2"] = sat_list[random2]["number"]
            l["s_3"] = sat_list[random3]["number"]
        with open("launches.json", "w") as outfile:
            outfile.write(json.dumps(launch_items, indent=4))

    # get instances of launches for satellites
    # 3 random for the month of specific satellite
    def get_launches_for_satellites(self):
        launch_items = json.load(open("launches.json"))
        satellite_items = json.load(open("satellite.json"))
        for s in satellite_items:
            month = s["month"]
            l_list = []
            for l in launch_items:
                if l["month"] == month:
                    l_list.append(l)
                elif int(month) < int(l["month"]):
                    break
            l_size = len(l_list)
            random1 = randrange(0, l_size)
            s["l_1"] = l_list[random1]["flight_number"]
            random2 = randrange(0, l_size)
            while random1 == random2:
                random2 = randrange(0, l_size)
            random3 = randrange(0, l_size)
            while random1 == random3 or random2 == random3:
                random3 = randrange(0, l_size)
            s["l_2"] = l_list[random2]["flight_number"]
            s["l_3"] = l_list[random3]["flight_number"]
        with open("satellite.json", "w") as outfile:
            outfile.write(json.dumps(satellite_items, indent=4))

    # get instances of satellites for planets and moons
    # 3 satellites per planets/moons
    def get_satellites_by_orbit(self, item):
        items = json.load(open(item + ".json"))
        satellite_items = json.load(open("satellite.json"))
        s_size = len(satellite_items)
        # get 3 random satellites for each planet
        # compare orbital periods
        for i in items:
            orbital = i["sideralOrbit"]
            random1 = randrange(0, s_size)
            random2 = randrange(0, s_size)
            # in the very off chance that randoms are the same loop until not
            while random1 == random2:
                random2 = randrange(0, s_size)
            random3 = randrange(0, s_size)
            while random1 == random3 or random2 == random3:
                random3 = randrange(0, s_size)
            i["s_1"] = satellite_items[random1]["number"]
            i["so_1"] = float(satellite_items[random1]["orbital_period"])
            i["s_2"] = satellite_items[random2]["number"]
            i["so_2"] = float(satellite_items[random2]["orbital_period"])
            i["s_3"] = satellite_items[random3]["number"]
            i["so_3"] = float(satellite_items[random3]["orbital_period"])
            i["so_diff_1"] = round(orbital / i["so_1"], 4)
            i["so_diff_2"] = round(orbital / i["so_2"], 4)
            i["so_diff_3"] = round(orbital / i["so_3"], 4)
        with open(item + ".json", "w") as outfile:
            outfile.write(json.dumps(items, indent=4))

    def print_satellite_countries(self):
        satellite_items = json.load(open("satellite.json"))
        countries = set()
        for s in satellite_items:
            country = s["country"]
            if country not in countries:
                countries.add(country)

        sorted_countries = sorted(countries)
        print("{[", end="")
        for c in sorted_countries:
            print('"' + c + '"' + ", ", end="")
        print("]}")

    def get_min_max_satellite_year(self):
        satellite_items = json.load(open("satellite.json"))
        min_year = 9999
        max_year = 0
        for s in satellite_items:
            date = s["launch_date"]
            year = int(date[0:4])
            min_year = min(min_year, year)
            max_year = max(max_year, year)

        print("min year: " + str(min_year))
        print("max year: " + str(max_year))

    def print_launch_ships(self):
        launch_items = json.load(open("launches.json"))
        ships = set()
        for l in launch_items:
            num_ships = len(l["ships"])
            if num_ships not in ships:
                ships.add(num_ships)

        sorted_ships = sorted(ships)
        print("{[", end="")
        for s in sorted_ships:
            print('"' + str(s) + '"' + ", ", end="")
        print("]}")


data = SpaceData()
# data.get_satellite_info()
# data.get_launches()
# data.get_planets()
# data.get_moons()
# data.get_images("moons")
# data.get_images("planets")
# data.get_satellite_images()
# data.append_date_month_launches()
# data.append_date_month_satellites()
# data.sort_by_month("satellite")
# data.sort_by_month("launches")
# data.get_satellites_for_launch()
# data.get_launches_for_satellites()
# data.get_satellite_country_names()
# data.get_satellites_by_orbit("planets")
# data.get_satellites_by_orbit("moons")
# data.print_satellite_countries()
# data.get_min_max_satellite_year()
# data.print_launch_ships()
