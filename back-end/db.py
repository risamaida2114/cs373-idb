# reads json files and creates model instances to be fed into the database

import json
from models import app, db, PlanetMoon, Satellite, Launch

# Reset the database
def reset_db():
    print("reset db called")
    db.session.remove()
    db.drop_all()
    db.create_all()
    print("finished resetting")


def populate_db():
    print("populate db called")
    populate_satellites()
    populate_launches()
    populate_planets_moons()
    print("finished populating")


def populate_body(filename):
    json_file = filename
    with (open(json_file)) as f:
        data = json.load(f)
        for body in data:
            mass = 0
            discovery_date = body["discoveryDate"]
            if body["mass"] != None:
                mass = body["mass"]["massValue"]
                massExponent = body["mass"]["massExponent"]
                mass = mass * (10**massExponent)
            if discovery_date == "":
                discovery_date = "Unknown"
            db_row = dict(
                {
                    "id": body["id"],
                    "name": body["englishName"],
                    "mass": mass,
                    "density": body["density"],
                    "gravity": body["gravity"],
                    "mean_radius": body["meanRadius"],
                    "discovery_date": discovery_date,
                    "body_type": body["bodyType"],
                    "image": body["image"],
                    "discoveredBy": body["discoveredBy"],
                    "avgTemp": body["avgTemp"],
                    "satellite_1": body["s_1"],
                    "satellite_2": body["s_2"],
                    "satellite_3": body["s_3"],
                    "satellite_orbit_1": body["so_1"],
                    "satellite_orbit_2": body["so_2"],
                    "satellite_orbit_3": body["so_3"],
                    "orbit_diff_1": body["so_diff_1"],
                    "orbit_diff_2": body["so_diff_2"],
                    "orbit_diff_3": body["so_diff_3"],
                }
            )

            num_bodies = (
                db.session.query(PlanetMoon).filter(PlanetMoon.id == body["id"]).count()
            )
            assert num_bodies <= 1

            if num_bodies == 0:
                b = PlanetMoon(**db_row)
                db.session.add(b)


def populate_planets_moons():
    populate_body("planets.json")
    populate_body("moons.json")

    db.session.commit()


def populate_launches():
    json_file = "launches.json"
    with (open(json_file)) as f:
        data = json.load(f)
        for launch in data:
            # fill in unkown core success
            ret_core_success = launch["cores"][0]["landing_success"]
            core_success = "Unknown" if ret_core_success == None else ret_core_success

            # default launch image
            ret_launch_img = launch["links"]["flickr"]["original"]
            launch_image = (
                ("https://farm5.staticflickr.com/4174/33859521334_d75fa367d5_o.jpg")
                if ret_launch_img == []
                else ret_launch_img[0]
            )

            # static fire date
            ret_static_fire = launch["static_fire_date_utc"]
            static_fire_date = (
                "0000-00-00" if ret_static_fire == None else ret_static_fire[:10]
            )

            # crop details
            details = launch["details"]
            if details == None:
                details = "No details available"
            if len(details) > 300:
                split_details = details.split(". ")
                final_details = ""
                while len(final_details) < 300 and len(split_details) > 0:
                    final_details += split_details.pop(0) + ". "
                details = final_details

            db_row = dict(
                {
                    "name": launch["name"],
                    "flight_number": launch["flight_number"],
                    "date_utc": launch["date_utc"][:10],
                    "static_fire_date": static_fire_date,
                    "ships": len(launch["ships"]),
                    "cores_success": core_success,
                    "youtube_id": launch["links"]["youtube_id"],
                    "success": launch["success"],
                    "details": details,
                    "patch_image": launch["links"]["patch"]["large"],
                    "rocket_image": launch_image,
                    "reddit_thread": launch["links"]["reddit"]["launch"],
                    "article": launch["links"]["article"],
                    "wikipedia": launch["links"]["wikipedia"],
                    "reused": "false",
                    "satellite_1": launch["s_1"],
                    "satellite_2": launch["s_2"],
                    "satellite_3": launch["s_3"],
                }
            )

            if (
                launch["fairings"] is not None
                and launch["fairings"]["reused"] is not None
            ):
                db_row["reused"] = launch["fairings"]["reused"]

            num_launches = (
                db.session.query(Launch)
                .filter(Launch.flight_number == launch["flight_number"])
                .count()
            )
            assert num_launches <= 1

            if num_launches <= 0:
                l = Launch(**db_row)
                db.session.add(l)

    db.session.commit()


def populate_satellites():
    json_file = "satellite.json"
    with (open(json_file)) as f:
        data = json.load(f)
        for satellite in data:
            launch_date = (
                "Unknown"
                if satellite["launch_date"] == None
                else satellite["launch_date"][:10]
            )

            db_row = dict(
                {
                    "name": satellite["name"],
                    "number": satellite["number"],
                    "classification": satellite["classification"],
                    "launch_date": launch_date,
                    "country": satellite["country"],
                    "image": satellite["image"],
                    "type": satellite["type"],
                    "size": satellite["size"],
                    "orbital_period": float(satellite["orbital_period"]),
                    "categories": satellite["categories"],
                    "intldes": satellite["intldes"],
                    "launch_1": satellite["l_1"],
                    "launch_2": satellite["l_2"],
                    "launch_3": satellite["l_3"],
                }
            )

            num_satellites = (
                db.session.query(Satellite)
                .filter(Satellite.number == satellite["number"])
                .count()
            )
            assert num_satellites <= 1

            if num_satellites == 0:
                s = Satellite(**db_row)
                db.session.add(s)

    db.session.commit()


if __name__ == "__main__":  # pragma: no cover
    with app.app_context():
        # reset_db()
        populate_db()
        print("done!")
