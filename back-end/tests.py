import unittest
import requests

URL = "https://api.clear-skies.me"


class UnitTests(unittest.TestCase):
    def test_hello_world(self):
        resp = requests.get(URL)
        self.assertEqual(resp.text, "Clear Skies Backend")

    def test_satellites(self):
        resp = requests.get(URL + "/satellites")
        rjson = resp.json()
        self.assertIn("satellites", rjson)
        # there is something
        self.assertNotEqual(len(rjson["satellites"]), 0)
        # its not nothing
        self.assertNotEqual(rjson["satellites"][0], None)

    def test_planetmoon(self):
        resp = requests.get(URL + "/planetsmoons")
        rjson = resp.json()
        self.assertIn("planetsmoons", rjson)
        # there is something
        self.assertNotEqual(len(rjson["planetsmoons"]), 0)
        # its not nothing
        self.assertNotEqual(rjson["planetsmoons"][0], None)

    def test_launch(self):
        resp = requests.get(URL + "/launches")
        rjson = resp.json()
        self.assertIn("launches", rjson)
        # there is something
        self.assertNotEqual(len(rjson["launches"]), 0)
        # its not nothing
        self.assertNotEqual(rjson["launches"][0], None)

    def test_satellite_id(self):
        resp = requests.get(URL + "/satellites/733")
        rjson = resp.json()
        dictInfo = {
            "categories": "{}",
            "classification": "U",
            "country": "US",
            "image": "https://uphere-space.sfo2.digitaloceanspaces.com/images/satellites_numbers/default.webp",
            "intldes": "1964-002A",
            "launch_1": 144,
            "launch_2": 89,
            "launch_3": 19,
            "launch_date": "1964-01-19",
            "name": "THOR AGENA D R/B",
            "number": 733,
            "orbital_period": 100.53,
            "size": "LARGE",
            "type": "ROCKET BODY",
        }
        self.assertEqual(rjson["satellite"], dictInfo)

    def test_satellite_id_entry(self):
        resp = requests.get(URL + "/satellites/25544")
        rjson = resp.json()
        self.assertEqual(rjson["satellite"]["country"], "ISS")

    def test_launch_id(self):
        resp = requests.get(URL + "/launches/13")
        rjson = resp.json()
        dictInfo = {
            "article": "http://spacenews.com/38959spacex-delivers-thaicom-6-satellite-to-orbit/",
            "cores_success": "Unknown",
            "date_utc": "2014-01-06",
            "details": 'Second GTO launch for Falcon 9. The USAF evaluated launch data from this flight as part of a separate certification program for SpaceX to qualify to fly U.S. military payloads and found that the Thaicom 6 launch had "unacceptable fuel reserves at engine cutoff of the stage 2 second burnoff"',
            "flight_number": 13,
            "name": "Thaicom 6",
            "patch_image": "https://images2.imgbox.com/f5/fa/JvLWfNZz_o.png",
            "reddit_thread": "http://www.reddit.com/r/spacex/comments/1ujoc0",
            "reused": "false",
            "rocket_image": "https://farm9.staticflickr.com/8617/16789019815_f99a165dc5_o.jpg",
            "satellite_1": 32404,
            "satellite_2": 15494,
            "satellite_3": 17295,
            "ships": 0,
            "static_fire_date": "2013-12-28",
            "success": "true",
            "wikipedia": "https://en.wikipedia.org/wiki/Thaicom_6",
            "youtube_id": "AnSNRzMEmCU",
        }
        self.assertEqual(rjson["launch"], dictInfo)

    def test_launch_id_entry(self):
        resp = requests.get(URL + "/launches/1")
        rjson = resp.json()
        self.assertEqual(rjson["launch"]["name"], "FalconSat")

    def test_planetmoon_id(self):
        resp = requests.get(URL + "/planetsmoons/1000")
        rjson = resp.json()
        dictInfo = {
            "average_temperature": 76,
            "body_type": "Planet",
            "density": 1.27,
            "discoveredBy": "William Herschel",
            "discovery_date": "13/03/1781",
            "gravity": 8.87,
            "id": 1000,
            "image": "https://upload.wikimedia.org/wikipedia/commons/c/c9/Uranus_as_seen_by_NASA%27s_Voyager_2_%28remastered%29_-_JPEG_converted.jpg",
            "mass": 8.68127e25,
            "mean_radius": 25362.0,
            "name": "Uranus",
            "orbit_diff_1": 316.1162,
            "orbit_diff_2": 295.3359,
            "orbit_diff_3": 21.3682,
            "satellite_1": 18749,
            "satellite_2": 44517,
            "satellite_3": 32478,
            "satellite_orbit_1": 97.07,
            "satellite_orbit_2": 103.9,
            "satellite_orbit_3": 1436.03,
        }
        self.assertEqual(rjson["planetmoon"], dictInfo)

    def test_planetmoon_id_entry(self):
        resp = requests.get(URL + "/planetsmoons/1000")
        rjson = resp.json()
        self.assertEqual(rjson["planetmoon"]["gravity"], 8.87)

    def test_site_search(self):
        resp = requests.get(URL + "/search?search=US")
        rjson = resp.json()

        self.assertEqual(rjson["satellites"][0]["number"], 733)
        self.assertEqual(rjson["launches"][0]["flight_number"], 13)
        self.assertEqual(rjson["planetsmoons"][0]["id"], 1000)

    def test_satellite_search(self):
        resp = requests.get(URL + "/satellites?search=EUTELSAT")
        rjson = resp.json()

        self.assertEqual(rjson["satellites"][0]["number"], 28946)

    def test_satellite_sort(self):
        resp = requests.get(URL + "/satellites?sort=launch_date")
        rjson = resp.json()

        self.assertEqual(rjson["satellites"][0]["number"], 694)

    def test_satellite_filter(self):
        resp = requests.get(URL + "/satellites?orbper=200-300")
        rjson = resp.json()

        self.assertEqual(rjson["satellites"][0]["number"], 8820)

    def test_launch_search(self):
        resp = requests.get(URL + "/launches?search=starlink")
        rjson = resp.json()

        self.assertEqual(rjson["launches"][0]["flight_number"], 87)

    def test_launch_sort(self):
        resp = requests.get(URL + "/launches?sort=static_fire_date")
        rjson = resp.json()

        self.assertEqual(rjson["launches"][0]["flight_number"], 101)

    def test_launch_filter(self):
        resp = requests.get(URL + "/launches?ships=4")
        rjson = resp.json()

        self.assertEqual(rjson["launches"][0]["flight_number"], 113)

    def test_planetsmoons_search(self):
        resp = requests.get(URL + "/planetsmoons?search=mars")
        rjson = resp.json()

        self.assertEqual(rjson["planetsmoons"][0]["id"], 1003)

    def test_planetsmoons_sort(self):
        resp = requests.get(URL + "/planetsmoons?sort=mass&desc=true")
        rjson = resp.json()

        self.assertEqual(rjson["planetsmoons"][0]["id"], 1002)

    def test_planetsmoons_filter(self):
        resp = requests.get(URL + "/planetsmoons?temp=100-150")
        rjson = resp.json()

        self.assertEqual(rjson["planetsmoons"][0]["id"], 1005)


if __name__ == "__main__":
    unittest.main()
