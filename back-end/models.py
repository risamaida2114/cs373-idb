# class definitions for our different models

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

app = Flask(__name__)
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql://clearskies:clearskies10@space-db.chbld91oomxa.us-east-2.rds.amazonaws.com/space-db"
CORS(app)

db = SQLAlchemy(app)

# Reset the database
def reset_db():
    print("reset db called")
    db.session.remove()
    db.drop_all()
    db.create_all()
    print("finished resetting")


class Satellite(db.Model):
    __tablename__ = "satellites"

    number = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    classification = db.Column(db.String(10))
    launch_date = db.Column(db.String(200))
    country = db.Column(db.String(200))
    image = db.Column(db.String(500))
    type = db.Column(db.String(100))
    size = db.Column(db.String(100))
    orbital_period = db.Column(db.Float)
    categories = db.Column(db.String(500))
    intldes = db.Column(db.String(10))
    launch_1 = db.Column(db.Integer)
    launch_2 = db.Column(db.Integer)
    launch_3 = db.Column(db.Integer)

    def __init__(
        self,
        name,
        number,
        classification,
        launch_date,
        country,
        image,
        type,
        size,
        orbital_period,
        categories,
        intldes,
        launch_1,
        launch_2,
        launch_3,
    ):
        self.name = name
        self.number = number
        self.classification = classification
        self.launch_date = launch_date
        self.country = country
        self.image = image
        self.type = type
        self.size = size
        self.orbital_period = orbital_period
        self.categories = categories
        self.intldes = intldes
        self.launch_1 = launch_1
        self.launch_2 = launch_2
        self.launch_3 = launch_3

    def __repr__(self) -> str:
        return f"<Satellite {self.name}>"

    def serialize(self):
        return {
            "name": self.name,
            "number": self.number,
            "classification": self.classification,
            "launch_date": self.launch_date,
            "country": self.country,
            "image": self.image,
            "type": self.type,
            "size": self.size,
            "orbital_period": self.orbital_period,
            "categories": self.categories,
            "intldes": self.intldes,
            "launch_1": self.launch_1,
            "launch_2": self.launch_2,
            "launch_3": self.launch_3,
        }


class Launch(db.Model):
    __tablename__ = "launches"

    flight_number = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    date_utc = db.Column(db.String(400))
    static_fire_date = db.Column(db.String(400))
    ships = db.Column(db.Integer)
    cores_success = db.Column(db.String(200))
    youtube_id = db.Column(db.String(500))
    success = db.Column(db.String(10))
    details = db.Column(db.String(1500))
    patch_image = db.Column(db.String(500))
    rocket_image = db.Column(db.String(2000))
    reddit_thread = db.Column(db.String(500))
    article = db.Column(db.String(500))
    wikipedia = db.Column(db.String(500))
    reused = db.Column(db.String(10))
    satellite_1 = db.Column(db.Integer)
    satellite_2 = db.Column(db.Integer)
    satellite_3 = db.Column(db.Integer)

    def __init__(
        self,
        flight_number,
        name,
        date_utc,
        static_fire_date,
        ships,
        cores_success,
        youtube_id,
        success,
        details,
        patch_image,
        rocket_image,
        reddit_thread,
        article,
        wikipedia,
        reused,
        satellite_1,
        satellite_2,
        satellite_3,
    ):
        self.flight_number = flight_number
        self.name = name
        self.date_utc = date_utc
        self.static_fire_date = static_fire_date
        self.ships = ships
        self.cores_success = cores_success
        self.youtube_id = youtube_id
        self.success = success
        self.details = details
        self.patch_image = patch_image
        self.rocket_image = rocket_image
        self.reddit_thread = reddit_thread
        self.article = article
        self.wikipedia = wikipedia
        self.reused = reused
        self.satellite_1 = satellite_1
        self.satellite_2 = satellite_2
        self.satellite_3 = satellite_3

    def __repr__(self) -> str:
        return f"<Launch {self.name}>"

    def serialize(self):
        return {
            "name": self.name,
            "flight_number": self.flight_number,
            "date_utc": self.date_utc,
            "static_fire_date": self.static_fire_date,
            "ships": self.ships,
            "cores_success": self.cores_success,
            "youtube_id": self.youtube_id,
            "success": self.success,
            "details": self.details,
            "patch_image": self.patch_image,
            "rocket_image": self.rocket_image,
            "reddit_thread": self.reddit_thread,
            "article": self.article,
            "wikipedia": self.wikipedia,
            "reused": self.reused,
            "satellite_1": self.satellite_1,
            "satellite_2": self.satellite_2,
            "satellite_3": self.satellite_3,
        }


class PlanetMoon(db.Model):
    __tablename__ = "planets_moons"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    mass = db.Column(db.Float)
    density = db.Column(db.Float)
    gravity = db.Column(db.Float)
    mean_radius = db.Column(db.Float)
    discovery_date = db.Column(db.String(200))
    body_type = db.Column(db.String(100))
    image = db.Column(db.String(500))
    discoveredBy = db.Column(db.String(500))
    avgTemp = db.Column(db.Integer)
    satellite_1 = db.Column(db.Integer)
    satellite_2 = db.Column(db.Integer)
    satellite_3 = db.Column(db.Integer)
    satellite_orbit_1 = db.Column(db.Float)
    satellite_orbit_2 = db.Column(db.Float)
    satellite_orbit_3 = db.Column(db.Float)
    orbit_diff_1 = db.Column(db.Float)
    orbit_diff_2 = db.Column(db.Float)
    orbit_diff_3 = db.Column(db.Float)

    def __init__(
        self,
        id,
        name,
        mass,
        density,
        gravity,
        mean_radius,
        discovery_date,
        body_type,
        image,
        discoveredBy,
        avgTemp,
        satellite_1,
        satellite_2,
        satellite_3,
        satellite_orbit_1,
        satellite_orbit_2,
        satellite_orbit_3,
        orbit_diff_1,
        orbit_diff_2,
        orbit_diff_3,
    ):
        self.id = id
        self.name = name
        self.mass = mass
        self.density = density
        self.gravity = gravity
        self.mean_radius = mean_radius
        self.discovery_date = discovery_date
        self.body_type = body_type
        self.image = image
        self.discoveredBy = discoveredBy
        self.avgTemp = avgTemp
        self.satellite_1 = satellite_1
        self.satellite_2 = satellite_2
        self.satellite_3 = satellite_3
        self.satellite_orbit_1 = satellite_orbit_1
        self.satellite_orbit_2 = satellite_orbit_2
        self.satellite_orbit_3 = satellite_orbit_3
        self.orbit_diff_1 = orbit_diff_1
        self.orbit_diff_2 = orbit_diff_2
        self.orbit_diff_3 = orbit_diff_3

    def __repr__(self) -> str:
        return f"<Body {self.name}>"

    def serialize(self):
        return {
            "id": self.id,
            "name": self.name,
            "mass": self.mass,
            "density": self.density,
            "gravity": self.gravity,
            "mean_radius": self.mean_radius,
            "discovery_date": self.discovery_date,
            "body_type": self.body_type,
            "image": self.image,
            "discoveredBy": self.discoveredBy,
            "average_temperature": self.avgTemp,
            "satellite_1": self.satellite_1,
            "satellite_2": self.satellite_2,
            "satellite_3": self.satellite_3,
            "satellite_orbit_1": self.satellite_orbit_1,
            "satellite_orbit_2": self.satellite_orbit_2,
            "satellite_orbit_3": self.satellite_orbit_3,
            "orbit_diff_1": self.orbit_diff_1,
            "orbit_diff_2": self.orbit_diff_2,
            "orbit_diff_3": self.orbit_diff_3,
        }


with app.app_context():
    db.create_all()
    print("finished creating models")
