// adapted from https://jestjs.io/docs/snapshot-testing
import renderer from 'react-test-renderer'
import { MemoryRouter } from 'react-router-dom'
import {shallow} from 'enzyme'
import Enzyme from 'enzyme'
import Adapter from '@cfaester/enzyme-adapter-react-18';
import { BrowserRouter as Router } from "react-router-dom";

import defaultImg from '../assets/member_photos/default.jpg';


import NavBar from '../components/NavBar'
import Home from '../components/Home'
import PaginateCards from '../components/PaginateCards'

import About from '../components/About/About';
import AboutCard from '../components/About/AboutCard'

import Launches from '../components/Launches/Launches'
import LaunchImageCard from '../components/Launches/LaunchImageCard'
import LaunchPage from '../components/Launches/LaunchPage';
import LaunchPageCard from '../components/Launches/LaunchPageCard';


import PlanetsAndMoons from '../components/PlanetsAndMoons/PlanetsMoons'
import PlanetAndMoonImageCard from '../components/PlanetsAndMoons/PlanetMoonImageCard'
import PlanetPage from '../components/PlanetsAndMoons/PlanetMoonPage';
import PlanetMoonPageCard from '../components/PlanetsAndMoons/PlanetMoonPageCard';

import Satellites from '../components/Satellites/Satellites'
import SatelliteImageCard from '../components/Satellites/SatelliteImageCard'
import SatellitePage from '../components/Satellites/SatellitePage'
import SatellitePageCard from '../components/Satellites/SatellitePageCard'

import DropDownSelect from '../components/Select';
import RangeSlider from '../components/RangeSlider';
import SearchBar from '../components/SearchBar';

Enzyme.configure({ adapter: new Adapter() });

it('about page initializes correctly', () => {
  const aboutPage = shallow(<Router><About /></Router>);
  expect(aboutPage).toMatchSnapshot();
});

it('home page initializes correctly', () => {
  const homePage = shallow(<Router><Home /></Router>);
  expect(homePage).toMatchSnapshot();
});

it("navbar initializes correctly", () => {
  const navbar = shallow(<Router><NavBar /></Router>);
  expect(navbar).toMatchSnapshot();
});

it('launches page initializes correctly', () => {
  const launch = shallow(<Router><Launches /></Router>);
  expect(launch).toMatchSnapshot();
});

it('launch instance page initializes correctly', () => {
  const launchPage = shallow(<Router><LaunchPage /></Router>);
  expect(launchPage).toMatchSnapshot();
});

it('planets and moons page initializes correctly', () => {
    const planetsmoons = shallow(<Router><PlanetsAndMoons /></Router>);
    expect(planetsmoons).toMatchSnapshot();
});

it('planet moon instance page initializes correctly', () => {
  const planetsmoonsPage = shallow(<Router><PlanetPage /></Router>);
  expect(planetsmoonsPage).toMatchSnapshot();
});

it('satellites page initializes correctly', () => {
  const satellites = shallow(<Router><Satellites /></Router>);
  expect(satellites).toMatchSnapshot();
});

it('satellite instance page initializes correctly', () => {
  const satellitesPage = shallow(<Router><SatellitePage /></Router>);
  expect(satellitesPage).toMatchSnapshot();
});

it('about card initializes correctly', () => {
  const aboutInfo = {
    name: "Name",
    image: defaultImg,
    bio: "Bio",
    responsibilities: "Responsibilities",
    commits: 0,
    issues: 0,
    unit_tests: 0
  }

  const tree = renderer
    .create(<AboutCard {...aboutInfo} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('launch image card initializes correctly', () => {
  const launchInfo = {
    date_utc: "2020-01-19",
    name: "Crew Dragon In Flight Abort Test",
    reused: "false",
    rocket_image: "https://live.staticflickr.com/65535/49421605028_b7ba890f0e_o.jpg",
    ships: "1",
    static_fire_date: "2020-01-11",
    success: "true"
  }

  const tree = renderer
    .create(<LaunchImageCard {...launchInfo} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('launch page card initializes correctly', () => {
  const launchInfo = {
    article: "https://spaceflightnow.com/2020/01/19/spacex-aces-final-major-test-before-first-crew-mission",
    cores_success: "Unknown",
    date_utc: "2020-01-19",
    details: "SpaceX will launch a Crew Dragon capsule from LC-39A, KSC on a fully fueled Falcon 9 rocket and then trigger the launch escape system during the period of maximum dynamic pressure. As part of NASA'a Commercial Crew Integrated Capability program (CCiCap) this test will contribute valuable data to help validate Crew Dragon and its launch abort system. ",
    flight_number: 88,
    name: "Crew Dragon In Flight Abort Test",
    patch_image: "https://images2.imgbox.com/19/df/IH0nVnSr_o.png",
    reddit_thread: "https://www.reddit.com/r/spacex/comments/eq24ap/rspacex_inflight_abort_test_official_launch",
    reused: "false",
    rocket_image: "https://live.staticflickr.com/65535/49421605028_b7ba890f0e_o.jpg",
    ships: 1,
    static_fire_date: "2020-01-11",
    success: "true",
    wikipedia: "https://en.wikipedia.org/wiki/Commercial_Crew_Development",
    youtube_id: "mhrkdHshb3E"
  }

  const tree = renderer
    .create(<LaunchPageCard {...launchInfo}/>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('planet moon image card initializes correctly', () => {
  const planetMoonInfo = {
    name: "Uranus",
    mass: "8.68127",
    density: "1.27",
    gravity: "8.87",
    mean_radius: "25362.0",
    discovery_date: "13/03/1781",
    image: "https://upload.wikimedia.org/wikipedia/commons/c/c9/Uranus_as_seen_by_NASA%27s_Voyager_2_%28remastered%29_-_JPEG_converted.jpg"
  }

  const tree = renderer
    .create(<PlanetAndMoonImageCard {...planetMoonInfo}/>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('planet moon page card initializes correctly', () => {
  const planetMoonInfo = {
    average_temperature: 76,
    body_type: "Planet",
    density: "1.27",
    discoveredBy: "William Herschel",
    discovery_date: "13/03/1781",
    gravity: "8.87",
    id: 1000,
    image: "https://upload.wikimedia.org/wikipedia/commons/c/c9/Uranus_as_seen_by_NASA%27s_Voyager_2_%28remastered%29_-_JPEG_converted.jpg",
    mass: "8.68127",
    mean_radius: "25362.0",
    name: "Uranus",
    orbit_diff_1: "316.1162",
    orbit_diff_2: "295.3359",
    orbit_diff_3: "21.3682",
    satellite_1: 18749,
    satellite_2: 44517,
    satellite_3: 32478,
    satellite_orbit_1: "97.07",
    satellite_orbit_2: "103.9",
    satellite_orbit_3: "1436.03"
  }

  const tree = renderer
    .create(<PlanetMoonPageCard {...planetMoonInfo}/>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('satellite image card initializes correctly', () => {
  const satInfo = {
    name: "DELTA 2 R/B(1)",
    size: "LARGE",
    type: "ROCKET BODY", 
    launch_date: "1990-01-24",
    country: "US",
    orbital_period: "96.56",
    image: "https://uphere-space.sfo2.digitaloceanspaces.com/images/satellites_numbers/default.webp"
  }

  const tree = renderer
    .create(<SatelliteImageCard {...satInfo}/>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('satellite page card initializes correctly', () => {
  const satInfo = {
    categories: "{BRIGHTEST}",
    classification: "U",
    country: "US",
    image: "https://uphere-space.sfo2.digitaloceanspaces.com/images/satellites_numbers/default.webp",
    intldes: "1990-008B",
    launch_1: 26,
    launch_2: 144,
    launch_3: 54,
    launch_date: "1990-01-24",
    name: "DELTA 2 R/B(1)",
    number: 20453,
    orbital_period: 96.56,
    size: "LARGE",
    type: "ROCKET BODY"
  }

  const tree = renderer
    .create(<SatellitePageCard {...satInfo}/>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('Drop Down Select created correctly', () => {
  const tree = renderer
    .create(
    <DropDownSelect
      displayTerm="Size"
      reqTerm="size"
      categories={["Small", "Medium", "Large"]}
    />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('Range Slider created correctly', () => {
  const tree = renderer
    .create(
      <RangeSlider
        displayTerm={"Launch Date"}
        reqTerm={"date"}
        filterVals={[1963, 2019]}
    />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
