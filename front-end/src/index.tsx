import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from './components/NavBar';
import About from './components/About/About';
import Home from './components/Home';
import Search from './components/Search';

import PlanetsAndMoons from './components/PlanetsAndMoons/PlanetsMoons';
import PlanetPage from './components/PlanetsAndMoons/PlanetMoonPage';
import Launches from './components/Launches/Launches';
import LaunchPage from './components/Launches/LaunchPage';
import Satellites from './components/Satellites/Satellites';
import SatellitePage from './components/Satellites/SatellitePage';
import VisualizationsPage from './components/Visualizations/Visualizations'
import OtherVisualizationsPage from './components/Visualizations/OtherVisualizations';

ReactDOM.render(
  <Router>
    <NavBar />
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/about" element={<About />} />
      <Route path="/search" element={<Search />} />

      <Route path="/planets-and-moons" element={<PlanetsAndMoons />} />
      <Route path="/planets-and-moons/:bodyId" element={<PlanetPage />} />

      <Route path="/launches" element={<Launches />} />
      <Route path="/launches/:launchId" element={<LaunchPage />} />

      <Route path="/satellites" element={<Satellites />} />
      <Route path="/satellites/:satelliteId" element={<SatellitePage />} />
      <Route path="/visualizations" element={<VisualizationsPage />} />
      <Route path="/provider-visualizations" element={<OtherVisualizationsPage />} />
    </Routes>
  </Router>,

  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log)) 
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();