import React from 'react';
import ParticlesContainer from './ParticlesContainer'
import './Home.css'
import Button from '@mui/material/Button';

function Home() {
  return (
    <div className = "main">
      <div className="background" style={{ zIndex : -1, position : "relative"}}> 
          <ParticlesContainer />
      </div>
      <div className="bg-text">
        Discover the Solar System.
      </div>
      <div className="subtext">
        Learn about planets, moons, and space exploration.
      </div>
      <div className="btn-container">
        <Button href="/search" style={{backgroundColor: "#f5f5f5", color: "#000000", fontSize: "1.2rem", fontWeight: "bold", width: "10rem", height: "3rem", marginTop: "2rem"}}>Search</Button>
      </div>
    </div>
  )
}

export default Home