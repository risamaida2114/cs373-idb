
import { Box, Typography } from '@mui/material';
import axios, { AxiosResponse } from 'axios';
import React, { useState, useEffect } from 'react';
import { ScatterChart, Scatter, XAxis, 
    YAxis, CartesianGrid, Label, Tooltip, ResponsiveContainer } from 'recharts';
  
const AgeFollowerScatter = () => {
    let [data, setData] = useState<any[]>([]);

    // get artist api info from provider api
    useEffect(() => {
        axios.get('https://api.concertfor.me/api/artists?n=200&offset=0')
        .then((response: AxiosResponse) => {
            let artists = response.data.artists;

            // process data
            let scatter_data: any[] = [];
            artists.forEach((artist: any) => {
              let age = -1;

              if(artist.birthday.length > 0) {
                const year = artist.birthday.substring(0, 4) * 1;
                age = new Date().getFullYear() - year;
              }

              // makes sure artist is alive, and checks for gender mostly to exclude bands, which mess up age
              if(age > 0 && age < 120 && artist.gender !== "Not Found") {
                scatter_data.push({
                  name: artist.artist_name,
                  followers: artist.followers,
                  age: age
                })
              }
            });

            setData(scatter_data);
        })
        .catch((error) => {
            console.log(error);
        });
    }, []);
    
    const toolTip = (props: any) => {
      const { active, payload } = props;
    
      if (active && payload !== null && payload.length > 0) {
        const data = payload[0].payload;
    
        return (
          <Box
            sx={{
              backgroundColor: "white",
              margin: 0,
              borderRadius: "16px",
              padding: "16px",
              border: "1px solid black"
            }}
          >
            <Typography>{"Artist: " + data.name}</Typography>
            <Typography>
              {"Age: " + data.age}
            </Typography>
            <Typography>{"Followers: " + data.followers.toLocaleString()}</Typography>
          </Box>
        );
      }
    
      return null;
    };

    const FormatY = (number) => {
      if(number >= 1000000){
        return (number/1000000).toLocaleString() + 'M';
      }else{
        return number.toLocaleString();
      }
    }
  
    return (
      <div>
        <Typography className="modelTitle" variant="h4" align="center" marginBottom="1em">
          Artist Age Compared to Follower Count
        </Typography>
        <ResponsiveContainer width="80%" aspect={2}>
          <ScatterChart width={800} height={600}>
              <CartesianGrid />
              <XAxis type="number" dataKey="age">
                  <Label
                      value="Age"
                      offset={-4}
                      position="insideBottom"
                      style={{ textAnchor: "middle" }}
                  />
              </XAxis>
              <YAxis type="number" dataKey="followers" width={120} tickFormatter={FormatY}>
                  <Label
                      angle={-90}
                      offset = {20}
                      value="Followers"
                      position="insideLeft"
                      style={{ textAnchor: "middle" }}
                  />

              </YAxis>
              <Tooltip content={toolTip} />
              <Scatter data={data} fill="#0088FE" />
          </ScatterChart>
        </ResponsiveContainer>
        </div>
    );
}
  
export default AgeFollowerScatter;