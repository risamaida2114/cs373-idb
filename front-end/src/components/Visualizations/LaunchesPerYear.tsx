import { Typography } from '@mui/material';
import axios, { AxiosResponse } from 'axios';
import React from 'react';
import { useState, useEffect } from 'react'
import { BarChart, Bar, CartesianGrid, XAxis, YAxis, Label, Tooltip, ResponsiveContainer } from 'recharts';
  
function LaunchesPerYear() {
    let [data, setData] = useState<any[]>([]);

    // Fetch launches data from API and create data for chart 
    // (number of launches per year)
    useEffect(() => {
        axios.get("https://api.clear-skies.me/launches")
        .then((response: AxiosResponse) => {
            let launches = response.data.launches;
            let years = new Set();
            let launchesPerYear: any[] = [];

            launches.forEach((launch: any) => {
                let year = launch.date_utc.substring(0, 4);
                years.add(year);
            });

            years.forEach((year: any) => {
                let launchesInYear = launches.filter((launch: any) => {
                    return launch.date_utc.substring(0, 4) === year;
                });
                launchesPerYear.push({year: year, launches: launchesInYear.length});
            });
            
            // Sort by year
            launchesPerYear.sort((a, b) => {
                return parseInt(a.year) - parseInt(b.year);
            });

            setData(launchesPerYear);
            console.log(data);
        })
        .catch((error) => {
            console.log(error);
        });
    }, []);
  
    return (
        <div>
            <Typography className="modelTitle" variant="h4" align="center" marginBottom="1em">
                Launches Per Year
            </Typography>
            <ResponsiveContainer width="60%" aspect={1.4}>
                <BarChart data={data}>
                    <Bar dataKey="launches" fill="#0088FE"/>
                    <CartesianGrid strokeDasharray="3 3" />
                    
                    <XAxis dataKey="year" >
                        <Label
                            value="Year"
                            offset= {-4}
                            position="insideBottom"
                            style={{ textAnchor: "middle" }}
                        />
                    </XAxis>
                    <YAxis>
                        <Label
                            angle={-90}
                            offset = {6}
                            value="Launches"
                            position="insideLeft"
                            style={{ textAnchor: "middle" }}
                        />
                    </YAxis>
                    <Tooltip />
                </BarChart>
            </ResponsiveContainer>
        </div>
    );
}
  
export default LaunchesPerYear;