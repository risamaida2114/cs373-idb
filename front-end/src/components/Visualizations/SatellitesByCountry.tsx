import { Box, Typography } from '@mui/material';
import axios, { AxiosResponse } from 'axios';
import React from 'react';
import { useState, useEffect } from 'react'
import { PieChart, Pie, Sector, Cell, ResponsiveContainer, Tooltip } from 'recharts';
  
function SatellitesByCountry() {
    let [data, setData] = useState<any[]>([]);

    // Fetch launches data from API and create data for chart 
    useEffect(() => {
        axios.get("https://api.clear-skies.me/satellites")
        .then((response: AxiosResponse) => {
            let satellites = response.data.satellites;
            let countries = new Set();
            let satInCountry: any[] = [];

            satellites.forEach((sat: any) => {
                let country = sat.country;
                countries.add(country);
            });

            satInCountry.push({country: 'Other', countries: 0});

            countries.forEach((c: any) => {
                let satInCount = satellites.filter((sat: any) => {
                    return sat.country === c;
                });

                if(satInCount.length > 9) {
                    satInCountry.push({country: c, countries: satInCount.length});
                }
                else {
                    satInCountry[0].countries += satInCount.length;
                }                
            });
            
            // Sort by year
            satInCountry.sort((a, b) => {
                return parseInt(a.year) - parseInt(b.year);
            });

            setData(satInCountry);
            console.log(data);
        })
        .catch((error) => {
            console.log(error);
        });
    }, []);

    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#E87EA1'];

    const toolTip = (props: any) => {
        const { active, payload } = props;
        
        if (active && payload !== null && payload.length > 0) {
            const data = payload[0].payload;
        
            return (
            <Box
                sx={{
                backgroundColor: "white",
                margin: 0,
                borderRadius: "16px",
                padding: "16px",
                border: "1px solid black"
                }}
            >
                <Typography>{"Country: " + data.country}</Typography>
                <Typography>{"Count: " + data.countries.toLocaleString()}</Typography>
            </Box>
            );
        }
        
        return null;
    };

    let renderLabel = function(entry) {
        return entry.country;
    }    
  
    return (
      <div>
        <Typography className="modelTitle" variant="h4" align="center">
          Number of Satellites by Country
        </Typography>
        <ResponsiveContainer width="50%" aspect={1}>
            <PieChart>
                <Pie 
                    dataKey="countries"
                    nameKey="country"
                    isAnimationActive={true}
                    data={data}
                    cx="50%"
                    cy="50%"
                    fill="#8884d8"
                    label={renderLabel} >
                        {data.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                        ))}
                </Pie>
                <Tooltip content={toolTip} />
            </PieChart>
        </ResponsiveContainer>
        </div>
    );
}
  
export default SatellitesByCountry;