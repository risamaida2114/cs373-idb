import React from 'react'
import AgeFollowerScatter from './AgeFollowerScatter'
import ArtistByCountryPie from './ArtistByCountryPie'
import GenreByPopularityBar from './GenreByPopularityBar'
import './Visualizations.css'

function OtherVisualizations() {
  return (
    <div style={{display: 'flex', justifyContent: 'center', flexDirection: 'column'}}>
      <div style={{justifyContent: 'center', paddingTop: '60px'}}><AgeFollowerScatter/></div>
      <div style={{justifyContent: 'center', paddingTop: '60px'}}><ArtistByCountryPie/></div>
      <div style={{justifyContent: 'center', paddingTop: '60px'}}><GenreByPopularityBar/></div>
    </div>
  )
}

export default OtherVisualizations