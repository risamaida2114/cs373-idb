import React from 'react'
import LaunchesPerYear from './LaunchesPerYear'
import SatellitesByCountry from './SatellitesByCountry'
import PMScatter from './PMScatter'

function Visualizations() {
  return (
    <div style={{display: 'flex', justifyContent: 'center', flexDirection: 'column'}}>
    <div style={{justifyContent: 'center', paddingTop: '60px'}}><LaunchesPerYear/></div>
    <div style={{justifyContent: 'center', paddingTop: '60px'}}><SatellitesByCountry/></div>
    <div style={{justifyContent: 'center', paddingTop: '60px'}}><PMScatter/></div>
  </div>
    
  )
}

export default Visualizations