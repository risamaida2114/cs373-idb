
import { Box, Typography } from '@mui/material';
import axios, { AxiosResponse } from 'axios';
import React, { useState, useEffect } from 'react';
import { ScatterChart, Scatter, XAxis, 
    YAxis, CartesianGrid, Label, Tooltip, ResponsiveContainer } from 'recharts';

const PMScatter = () => {
    let [data, setData] = useState<any[]>([]);

    // get artist api info from provider api
    useEffect(() => {
        axios.get("https://api.clear-skies.me/planetsmoons")
        .then((response: AxiosResponse) => {
            let planetsmoons = response.data.planetsmoons;
            let scatter_data: any[] = [];

            planetsmoons.forEach((pm: any) =>{
                if(pm.gravity > 0 && pm.density > 0){
                    scatter_data.push({
                        name: pm.name,
                        gravity: pm.gravity,
                        density: pm.density
                    })
                }
            })
            console.log(scatter_data);
            setData(scatter_data);
        })
        .catch((error) => {
            console.log(error);
        });
    }, []);
    
    const toolTip = (props: any) => {
      const { active, payload } = props;
    
      if (active && payload !== null && payload.length > 0) {
        const data = payload[0].payload;
    
        return (
          <Box
            sx={{
              backgroundColor: "white",
              margin: 0,
              borderRadius: "16px",
              padding: "16px",
              border: "1px solid black"
            }}
          >
            <Typography>{"Name: " + data.name}</Typography>
            <Typography>
              {"Density " + data.density.toLocaleString()}
            </Typography>
            <Typography>{"Gravity: " + data.gravity.toLocaleString()}</Typography>
          </Box>
        );
      }
    
      return null;
    };

    const FormatY = (number) => {
      if(number >= 1000000){
        return (number/1000000).toLocaleString() + 'M';
      }else{
        return number.toLocaleString();
      }
    }
  
    return (
      <div>
        <Typography className="modelTitle" variant="h4" align="center" marginBottom="1em">
          Planets and Moons Density Compared to Gravity
        </Typography>
        <ResponsiveContainer width="80%" aspect={2}>
          <ScatterChart width={800} height={600}>
              <CartesianGrid />
              <XAxis type="number" dataKey="density">
                  <Label
                      value="density"
                      offset={-4}
                      position="insideBottom"
                      style={{ textAnchor: "middle" }}
                  />
              </XAxis>
              <YAxis type="number" dataKey="gravity" width={120} tickFormatter={FormatY}>
                  <Label
                      angle={-90}
                      offset = {24}
                      value="gravity"
                      position="insideLeft"
                      style={{ textAnchor: "middle" }}
                  />

              </YAxis>
              <Tooltip content={toolTip} />
              <Scatter data={data} fill="#0088FE" />
          </ScatterChart>
        </ResponsiveContainer>
        </div>
    );
}

export default PMScatter;