
import { Box, Typography } from '@mui/material';
import axios, { AxiosResponse } from 'axios';
import React, { useState, useEffect } from 'react';
import { Tooltip, BarChart, CartesianGrid, XAxis, YAxis, Bar, ResponsiveContainer } from 'recharts';

    
    
const GenreByPopularityBar = () => {
    let [data, setData] = useState<any[]>([]);

    // get artist api info from provider api
    useEffect(() => {
        axios.get('https://api.concertfor.me/api/artists?n=200&offset=0')
        .then((response: AxiosResponse) => {
            let artists = response.data.artists;

            // process data
            let genre_stats: { [key: string]: any[] } = {};
            let final_stats: { [key: string]: number } = {};

            artists.forEach((artist: any) => {
                // get list of genres applicable to artist
                let genre_list = [artist.genre];

                // loop through provided genres add artists popularity to dictionary list
                for(let i in genre_list) {
                    let genre = genre_list[i].toLowerCase();
                
                    if (genre in genre_stats) {
                        genre_stats[genre].push(artist.popularity);
                    }
                    else {
                        genre_stats[genre] = [artist.popularity];
                    }
                }
            });

            // calculate average popularity of each genre
            for(let genre in genre_stats) {
                if(genre_stats[genre].length > 1) {
                    const sum = genre_stats[genre].reduce((a, b) => a + b)
                    const average = sum / genre_stats[genre].length;

                    final_stats[genre] = average;
                }
            }
            
            // make bar groups
            let groups : any[] = [];
            Object.entries(final_stats).map( ([key, value]) => {
                groups.push({"genre": key, "popularity": value})
            });

            // attempt to sort list
            groups.sort(function(a, b) {
                return b.popularity - a.popularity;
            });

            setData(groups);
        })
        .catch((error) => {
            console.log(error);
        });
    }, []);
  
    return (
      <div>
        <Typography className="modelTitle" variant="h4" align="center" marginBottom="1em">
          Genre By Average Popularity
        </Typography>
        <ResponsiveContainer width="90%" aspect={2.5}>
            <BarChart
                width={800}
                height={400}
                data={data}>
                    <CartesianGrid />
                    <XAxis dataKey="genre" />
                    <YAxis />
                    <Tooltip />
                    <Bar dataKey="popularity" fill='#0088FE'/>
            </BarChart>
        </ResponsiveContainer>
        </div>
    );
}
  
export default GenreByPopularityBar;