
import { Box, Typography } from '@mui/material';
import axios, { AxiosResponse } from 'axios';
import React, { useState, useEffect } from 'react';
import { PieChart, Cell, Pie, Tooltip, ResponsiveContainer } from 'recharts';
    
const ArtistByCountryPie = () => {
    let [data, setData] = useState<any[]>([]);

    // get artist api info from provider api
    useEffect(() => {
        axios.get('https://api.concertfor.me/api/artists?n=200&offset=0')
        .then((response: AxiosResponse) => {
            let artists = response.data.artists;

            // process data
            let country_stats: { [key: string]: number } = {};
            let final_stats: { [key: string]: number } = {};

            artists.forEach((artist: any) => {
                // get country
                let country = artist.home_country;

                if(country !== "Not Found") {
                    if (country in country_stats) {
                        country_stats[country] += 1;
                    }
                    else {
                        country_stats[country] = 1;
                    }
                }                
            });

            // countries with only 1 artist will be merged into other
            final_stats["Other"] = 0;
            for (let country in country_stats) {
                if (country_stats[country] > 3) {
                    final_stats[country] = country_stats[country];
                }
                else {
                    final_stats["Other"] += 1;
                }
            }

            // make pie groups
            let groups : any[] = [];
            Object.entries(final_stats).map( ([key, value]) => {
                groups.push({"country": key, "count": value})
            });
            setData(groups);
        })
        .catch((error) => {
            console.log(error);
        });
    }, []);
    
    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#E87EA1'];

    const toolTip = (props: any) => {
        const { active, payload } = props;
        
        if (active && payload !== null && payload.length > 0) {
            const data = payload[0].payload;
        
            return (
            <Box
                sx={{
                backgroundColor: "white",
                margin: 0,
                borderRadius: "16px",
                padding: "16px",
                border: "1px solid black"
                }}
            >
                <Typography>{"Country: " + data.country}</Typography>
                <Typography>{"Count: " + data.count.toLocaleString()}</Typography>
            </Box>
            );
        }
        
        return null;
    };

    let renderLabel = function(entry) {
        return entry.country;
    }    
  
    return (
      <div>
        <Typography className="modelTitle" variant="h4" align="center">
          Number of Artists by Country
        </Typography>
        <ResponsiveContainer width="50%" aspect={1}>
            <PieChart width={500} height={500}>
                <Pie 
                    dataKey="count"
                    nameKey="country"
                    isAnimationActive={true}
                    data={data}
                    cx="50%"
                    cy="50%"
                    fill="#8884d8"
                    label={renderLabel} >
                        {data.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                        ))}
                </Pie>
                <Tooltip content={toolTip} />
            </PieChart>
        </ResponsiveContainer>
        </div>
    );
}
  
export default ArtistByCountryPie;