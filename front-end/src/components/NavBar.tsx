import React from 'react'
import { NavLink } from "react-router-dom";
import 'bootstrap/dist/js/bootstrap.bundle';

function NavBar() {
  return (
    <div className="NavBar" style={{ zIndex : 1 }}> 
        <nav className="navbar navbar-expand-md navbar-dark bg-dark">
        <div className="container" style={{flexDirection:'row' }}>
            <NavLink className="navbar-brand" to="/">
            Clear Skies
            </NavLink>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav ms-auto">
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/">
                            Home
                            <span className="sr-only"></span>
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/search">
                            Search
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/planets-and-moons">
                            Planets and Moons
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/launches">
                            Launches
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/satellites">
                            Satellites
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/visualizations">
                            Visualizations
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/provider-visualizations">
                            Provider Visualizations
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/about">
                            About Us
                        </NavLink>
                    </li>
                </ul>
            </div>
        </div>
        </nav>
    </div>
  )
}

export default NavBar