import React, { useEffect, useState } from 'react';

/* adapted from https://codesandbox.io/s/jt4dh?file=/src/styles.css:0-1403 */

type DropDownProps = {
  attributes: string[];
  showDropDown: boolean;
  toggleDropDown: Function;
  sortSelection: Function;
};

const DropDown: React.FC<DropDownProps> = ({
  attributes,
  sortSelection,
}: DropDownProps): JSX.Element => {
  const [showDropDown, setShowDropDown] = useState<boolean>(false);

  const onClickHandler = (city: string): void => {
    sortSelection(city);
  };

  useEffect(() => {
    setShowDropDown(showDropDown);
  }, [showDropDown]);

  return (
    <>
      <div className={showDropDown ? 'dropdown' : 'dropdown active'}>
        {attributes.map(
          (sort: string, index: number): JSX.Element => {
            return (
              <p
                key={index}
                onClick={(): void => {
                  onClickHandler(sort);
                }}
              >
                {sort}
              </p>
            );
          }
        )}
      </div>
    </>
  );
};

export default DropDown;
