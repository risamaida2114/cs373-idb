import * as React from 'react';
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';
import Typography from '@mui/material/Typography';

function valuetext(value: number) {
  return `${value}°C`;
}

export default function RangeSlider({
    displayTerm,
    reqTerm,
    filterVals,
    req,
    setReq
  } : {
    displayTerm: string
    reqTerm: string
    filterVals: number[]
    req: string
    setReq: (value: string) => void
}) {
  const [value, setValue] = React.useState<number[]>([filterVals[0], filterVals[1]]);

  const handleChange = (event: Event, newValue: number | number[]) => {
    setValue(newValue as number[]);
    // if the request term is already in the request, replace it
    if (req.includes(reqTerm)) {
        const prevFilter = req.split('&').filter((term) => term.includes(reqTerm))[0];
        const newReq = req.replace(prevFilter, `${reqTerm}=${value[0]}-${value[1]}`);
        setReq(newReq);
    } else {
        // otherwise, add it to the request
        setReq(req + `&${reqTerm}=${value[0]}-${value[1]}`);
    }
  };

  return (
    <Box sx={{ width: 300 }}>
      <Typography id="input-slider" sx={{ mt: 2 }} gutterBottom>
        {displayTerm}
      </Typography>
      <Slider
        value={value}
        onChange={handleChange}
        valueLabelDisplay="auto"
        getAriaValueText={valuetext}
        min={filterVals[0]}
        max={filterVals[1]}
        sx={{ ml: 1}}
      />
    </Box>
  );
}