import {useState} from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';

export default function DropdownSelect({
  displayTerm,
  reqTerm,
  categories,
  req,
  setReq
} : {
  displayTerm: string
  reqTerm: string
  categories: string[]
  req: string
  setReq: (value: string) => void
}) {
  const [value, setValue] = useState<string>("");

  const handleChange = (event: SelectChangeEvent) => {
    setValue(event.target.value as string);
    
    // replace all spaces with underscores for the api request
    let term = event.target.value as string;
    term = term.replace(/ /g, "_");
    
    if (req.includes(reqTerm)) {
        const prevFilter = req.split('&').filter((term) => term.includes(reqTerm))[0];
        let newReq = "";
        if (term === "None") {
          newReq = req.replace('&' + prevFilter, '');
        } else {
          newReq = req.replace(prevFilter, `${reqTerm}=${term}`);
        }
        setReq(newReq);
    } else if (term !== "None") {
        setReq(req + `&${reqTerm}=${term}`);
    }
  };

  return (
    <Box sx={{ minWidth: 120, pt: 2 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">{displayTerm}</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={value}
          label={displayTerm}
          onChange={handleChange}
        >
          <MenuItem key={"None"} value={"None"}>{"None"}</MenuItem>
          {categories.map((val) => (
            <MenuItem key={val} value={val}>{val}</MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
}