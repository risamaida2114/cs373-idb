import React, { useEffect, useState } from 'react'
import axios from 'axios';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ConnectionCard(
  launchInfo : any,
) {
  const api_url = "https://api.clear-skies.me"
  const [name, setName] = useState("");
  const [image, setImage] = useState("");
  const [error, setError] = useState(null);

  useEffect(() => {
    axios(api_url + '/launches/' + launchInfo["id"])
    .then((response) => {
      setName(response.data.launch.name);
      setImage(response.data.launch.rocket_image);
      setError(null);
    })
      .catch((error));
  }, [launchInfo["id"]]);

  return (
    <Link to={"/launches/"+launchInfo["id"]}>
      <Card bg={'dark'} key={'dark'} text={'white'} className="">
          <Card.Img variant="top" src={image} />
          <Card.Footer style={{fontSize: '1.5rem'}}>{name}</Card.Footer>
      </Card>
    </Link>
  );
}
