import React from 'react'
import { useState, useEffect } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom';
import Row from 'react-bootstrap/Row';
import { Container } from 'react-bootstrap'
import PaginateCards from '../PaginateCards'
import SatelliteImageCard from './SatelliteImageCard';
import SearchBar from '../SearchBar';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import RangeSlider from '../RangeSlider';
import Box from '@mui/material/Box';
import DropDownSelect from '../Select';
import '../top-bar.css'
import '../ModelPage.css'

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

function Satellites() {
  const [satellites, setSatellites] = useState([] as any);
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [sort, setSort] = useState<string>("");
  const [metadata, setMetadata] = useState([] as any);
  const [error, setError] = useState(null);
  const sortableAttributes = ["Name", "Size", "Type", "Launch Date", "Country", "Orbital Period"]
  const [filters, setFilters] = useState<string>("");
  const [open, setOpen] = React.useState(false);
  const [pageIndex, setPageIndex] = useState(1);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  useEffect(() => {
    let search = searchTerm ? `&search=${searchTerm}` : "";
    let req = `https://api.clear-skies.me/satellites?page=${pageIndex}${sort}${search}${filters}`;

    console.log(req);

    axios(`${req}`)
    .then((response) => {
      setSatellites(response.data.satellites);
      setMetadata(response.data.metadata);
      setError(null);
    })
    .catch(setError);
  }, [pageIndex, searchTerm, sort, filters]);
  
    if (error) return <p>An error occurred in Satellites.tsx</p>

  const cards = satellites.map(({ name, number, size, type,
  launch_date, country, orbital_period, image }) => {
    return (
      <Link to={"/satellites/" + number}>
      <SatelliteImageCard 
        name={name}
        size={size}
        type={type}
        launch_date={launch_date}
        country={country}
        orbital_period={orbital_period.toString()}
        image={image}
        search={searchTerm}
      />
      </Link>
    )
  });

  return (
    <div>
      <div className="top-bar" style={{display : "flex", flexWrap: "wrap-reverse", justifyContent: "flex-end", marginRight: "5rem"}}>
      <div className="wrapper">
          <button onClick={handleOpen}>Filters</button>
              <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
              >
              <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">Select Filters</Typography>

                <DropDownSelect
                  displayTerm="Size"
                  reqTerm="size"
                  categories={["Small", "Medium", "Large"]}
                  req={filters}
                  setReq={setFilters}
                />

                <DropDownSelect
                  displayTerm="Type"
                  reqTerm="type"
                  categories={["Payload", "Rocket Body"]}
                  req={filters}
                  setReq={setFilters}
                />

                <DropDownSelect
                  displayTerm="Country"
                  reqTerm="country"
                  categories={["AB", "AC", "ARGN", "AUS", "BRAZ", "CA", "CIS", "DEN", "ESA", "EUME", "EUTE", "FR", "GER", "GLOB", "GREC", "IM", "IND", "INDO", "IRAN", "ISRA", "ISS", "IT", "ITSO", "JPN", "NETH", "NICO", "NOR", "ORB", "PRC", "SAUD", "SES", "SKOR", "SPN", "SWED", "THAI", "UAE", "UK", "US", "VTNM"]}
                  req={filters}
                  setReq={setFilters}
                />

                <RangeSlider
                  displayTerm={"Launch Date"}
                  reqTerm={"date"}
                  filterVals={[1963, 2019]}
                  req={filters}
                  setReq={setFilters}
                />

                <RangeSlider
                  displayTerm={"Orbital Period"}
                  reqTerm={"orbper"}
                  filterVals={[89, 676]}
                  req={filters}
                  setReq={setFilters}
                />

                <button onClick={handleClose} style={{float: "right"}}>Apply</button>
              </Box>
            </Modal>
        </div>

        <div className="sort" style={{paddingTop: "2.1rem", paddingRight : "1rem"}}>
            <DropDownSelect
              displayTerm="Sort By"
              reqTerm="sort"
              categories={sortableAttributes}
              req={sort}
              setReq={setSort}
            />
        </div>

        <SearchBar 
          placeholder="Search Launches"
          searchTerm={searchTerm}
          setSearchTerm={setSearchTerm}
        />
      </div>
      <Container>
        <Row><PaginateCards tiles={cards} total_tiles={metadata["total_satellites"] ?? 0 } pageParam={pageIndex} setPageParam={setPageIndex} /></Row>
      </Container>
    </div>
  );
}

export default Satellites
