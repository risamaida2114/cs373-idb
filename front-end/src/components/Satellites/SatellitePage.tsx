import ParticlesContainer from '../ParticlesContainer';
import React, { useEffect, useState } from 'react';
import SatellitePageCard from './SatellitePageCard';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import '../About/about.css';
import { Container, Row, Col, Card } from 'react-bootstrap';
import ConnectionCard from './ConnectionCard';
import ConnectionsGrid from './ConnectionsGrid';

export type Satellite = {
  name ?: string;
  number ?: number;
  classification ?: string;
  launch_date ?: string;
  country ?: string;
  image ?: string;
  type ?: string;
  size ?: string;
  orbital_period ?: string;
  categories ?: string;
  intldes ?: string;
  launch_1 ?: string;
  launch_2 ?: string;
  launch_3 ?: string;
};

function SatellitePage() {
  const {satelliteId} = useParams();
  const api_url = "https://api.clear-skies.me"
  const [satelliteData, setSatelliteData] = useState<Satellite>();
  const [name, setName] = useState("");
  const [error, setError] = useState(null);

  useEffect(() => {
    axios(api_url + '/satellites/' + satelliteId)
    .then((response) => {
      setSatelliteData(response.data.satellite);
      setName(response.data.satellite.name);
      setError(null);
    })
      .catch((error));
  }, [satelliteId]);

  const connectionCards = ["1", "2", "3"].map((num) => {
    // if body data is undefined
    if (!satelliteData) {
      return; 
    }

    const id = satelliteData["launch_" + num];
    return (
      <ConnectionCard {...{id}}/>
    )
  });  

  return (
    <div className='about'>
      <div className="background overflow-hidden" style={{ zIndex : -1, position : "relative"}}> 
        <ParticlesContainer />
      </div>
        <main className="scroll-container">
        <Container fluid style={{height: '100%'}}>
          <Row>
            <section>
              <div className='mt-5 text-center text-white'>
                <h1 className='' style={{fontSize: '4rem'}}>{name}</h1>
              </div>
            </section>
            <section>
              <div className=''>
                <SatellitePageCard {...satelliteData}/>
              </div>
            </section>
  
          </Row>
        </Container>
        <Container fluid style={{backgroundColor:'white', height: '100%'}}>
          <Row>
            <section>
              <div className='p-5 text-dark text-center mb-5'>
                <h1 className='mb-5'>Find Launches With Similar Launch Date</h1>
                <ConnectionsGrid tiles={connectionCards} />
              </div>
            </section>
            <Card></Card>
          </Row>
        </Container>

        </main>
    </div>
  )
}

export default SatellitePage;