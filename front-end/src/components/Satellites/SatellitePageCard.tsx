import Card from 'react-bootstrap/Card';
import ReactCardFlip from 'react-card-flip';
import { useState } from 'react';
import '../PageCard.css'
import Repeat from '../../assets/repeat.png';

export type Satellite = {
  name ?: string;
  number ?: number;
  classification ?: string;
  launch_date ?: string;
  country ?: string;
  image ?: string;
  type ?: string;
  size ?: string;
  orbital_period ?: string;
  categories ?: string;
  intldes ?: string;
};

export default function SatellitePageCard({
  name = "",
  number = 0,
  classification = "",
  launch_date = "",
  country = "",
  image = "",
  type = "",
  size = "",
  orbital_period = "",
  categories = "",
  intldes = "",
}: Satellite) {

  const [flip, setFlip] = useState(false);
  return (
      <ReactCardFlip isFlipped={flip} 
          flipDirection="horizontal">
        <Card
          key={'dark'}
          text={'white'}
          className="text-center card-front"
          style={{ width: '25rem', cursor: 'pointer'}}
          onClick={() => setFlip(!flip)}
        >
          <Card.Img variant='top' src={image}/>
            <Card.Footer className="footer-front container"> 
              Learn more. <img src={Repeat} width={25} height={25} alt="plus icon" className="plus-icon"/>
            </Card.Footer>
        </Card>
  
        <Card
          key={'dark'}
          text={'white'}
          className="text-left card-front"
          style={{ width: '25rem', cursor: 'pointer'}}
          onClick={() => setFlip(!flip)}
        >
          <Card.Body>
            <Card.Title style={{fontWeight: 'bold'}}>Additional Information</Card.Title>
                <p><b>Name: </b> {name} </p>
                <p><b>Number: </b> {number} </p>
                <p><b>Classification: </b> {classification} </p>
                <p><b>Launch Date: </b> {launch_date} </p>
                <p><b>Country: </b> {country} </p>
                <p><b>Type: </b> {type} </p>
                <p><b>Size: </b> {size} </p>
                <p><b>Orbital Period: </b> {orbital_period} </p>
                <p><b>Categories: </b> {categories} </p>
                <p><b>International Designator: </b> {intldes} </p>
            </Card.Body>
            <Card.Footer className="footer-front container"> 
              <img src={Repeat} width={25} height={25} alt="plus icon" className="plus-icon"/>
            </Card.Footer>
        </Card>
              
      </ReactCardFlip>
  );
}
