import React from 'react'
import Card from 'react-bootstrap/Card';
import 'react-slideshow-image/dist/styles.css'
import '../Card.css'
import Highlighter from "react-highlight-words";

export default function SatelliteImageCard({
  name = "",
  size = "",
  type = "", 
  launch_date = "",
  country = "",
  orbital_period = "",
  image = "",
  search = ""
}) {
  return (
    <Card
    bg={'dark'}
    key={'dark'}
    text={'white'}
    className="custom-card"
  >
    <Card.Img variant="top" src={image} />
    <Card.Body>
        <Card.Title>
          <Highlighter
            highlightClassName="highlighter"
            searchWords={search.split(" ") ?? []}
            autoEscape={true}
            textToHighlight= {name}
          />
        </Card.Title>
        <Card.Text>
          <p className="p3"><b>Size: </b> {size} </p>
          <p className="p3"><b>Satellite Type: </b> {type} </p>
          <p className="p3"><b>Launch Date: </b> 
            <Highlighter
              highlightClassName="highlighter"
              searchWords={search.split(" ") ?? []}
              autoEscape={true}
              textToHighlight= {launch_date}
            /> </p>
          <p className="p3"><b>Country: </b>
            <Highlighter
              highlightClassName="highlighter"
              searchWords={search.split(" ") ?? []}
              autoEscape={true}
              textToHighlight= {country}
            /></p>
          <p className="p3"><b>Orbital Period: </b> 
            <Highlighter
              highlightClassName="highlighter"
              searchWords={search.split(" ") ?? []}
              autoEscape={true}
              textToHighlight= {orbital_period}
            /> </p>
        </Card.Text>
    </Card.Body>
  </Card>
);
}
