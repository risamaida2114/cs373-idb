import React from 'react'
import { useState, useEffect } from 'react'
import axios from 'axios'
import mghanem_photo from '../../assets/member_photos/mghanem_photo.jpg'
import rmaida_photo from '../../assets/member_photos/rmaida_photo.jpg'
import asaini_photo from '../../assets/member_photos/asaini_photo.jpg'
import dsialm_photo from '../../assets/member_photos/dsialm_photo.jpg'
import ParticlesContainer from '.././ParticlesContainer'
import AboutCard from './AboutCard'
import AboutGrid from './AboutGrid'
import ListGroup from 'react-bootstrap/ListGroup';
import { Row } from 'react-bootstrap'
import './about.css'
import AboutToolCard from './AboutToolCard'
import Container from 'react-bootstrap/Container'

class GroupMember {
  name = "";
  username = "";
  photo = "";
  bio = "";
  responsibilities = "";
  commits = 0;
  issues = 0;
  unittests = 0;

  constructor(name, username, photo, bio, responsibilities, unittests) {
    this.name = name;
    this.username = username;
    this.photo = photo;
    this.bio = bio;
    this.responsibilities = responsibilities;
    this.unittests = unittests
  }
}

class Link {
  title = "";
  url = "";
  description = "";

  constructor(title, url, description) {
    this.title = title;
    this.url = url;
    this.description = description;
  }
}

const memberInfo = [
  new GroupMember('Moe Ghanem', 'moeghanem', mghanem_photo, "Hi, I'm Moe. I'm a Junior CS student from Highland Village, Texas. In my free time, you will find me coaching or bouldering at the local climbing gyms. Other than that, I am a big fan of cats!", 'Front-end Developer, API scraping', 0),
  new GroupMember('Risa Maida', 'risamaida2114', rmaida_photo, "Hey, I'm Risa. I'm a Junior Computer Science and Mathematics major from Houston, Texas. When I'm not doing CS, I enjoy reading books, drawing, and music!", 'Phase 1 Leader, Front-end Developer', 11),
  new GroupMember('Anshita Saini', 'anshitasaini', asaini_photo, "Hi! My name is Anshita and I'm a second-year Computer Science major at UT Austin originally from Seattle, Washington. I love to cook and I'm currently watching Silicon Valley.", 'Phase 2 Leader, Front-end Developer, API scraping', 0),
  new GroupMember('Daniel Sialm', 'daniel.sialm', dsialm_photo, "Hey, I'm Daniel, a Junior studying Computer Science and Math from Austin, Texas. I enjoy skiing, watching movies, and play on the Texas Quadball team.", 'Front-end Developer, API scraping', 36)
];

const apiInfo = [
  new Link('Satellites', 'https://uphere-space1.p.rapidapi.com/', 'Provides all information about satellites, as well as photos of them (if available). Many of the satellites return default images, but the information is updated with accurate orbital periods for satellites from all around the world.'),
  new Link('The Solar System OpenData', 'https://api.le-systeme-solaire.net/en/', 'Provides most of the information for planets and moons. The API contains a key value “bodyType.” Models are sorted by the respective bodyTypes: “Planet”, “Moon”, and “Asteroid”.Every instance listed is generated as a card on its respective model page.'),
  new Link('SpaceX Launches', 'https://api.spacexdata.com/v4/launches', 'This API for SpaceX launches has an abundant amount of information per launch. Because each launch is very well-documented, we were able to find launch images and videos for each one, as well as all of the information about the launch success and specific rocket details.'),
  new Link('GitLab', 'https://gitlab.com/risamaida2114/cs373-idb', 'The GitLab API was called in the front-end of our About page to retrieve the number of commits, issues, and unit tests per member.')
];


function About() {
  const [commitInfo, setCommitInfo] = useState([] as any);
  const [issueInfo, setIssueInfo] = useState([]);
  const [error, setError] = useState(null);

  // get commits from the main branch
  useEffect(() => {
    const getCommitList = async() => {
      let commit_list = [] as any;
      let page = [] as any;
      let pageNum = 1;
    
      do {
        page = await fetch(
          'https://gitlab.com/api/v4/projects/39614220/repository/commits?per_page=100&ref=main&page=' + pageNum
        );
    
        page = await page.json();
        commit_list = [...commit_list, ...page];
        pageNum += 1;    
      } while (page.length === 100);
    
      setCommitInfo(commit_list);
    };
    getCommitList();
  })

  // get closed issues
  useEffect(() => {
    axios('https://gitlab.com/api/v4/projects/39614220/issues?state=closed&per_page=100')
    .then((response) => {
      setIssueInfo(response.data);
      setError(null);
    })
    .catch(setError);
  }, []); 

  if (error) return <p>An error occurred in About.tsx</p>

  // add all of the commits to the member map
  var total_commits = 0;
  let commit_map = new Map();
  commitInfo.forEach((commit: any) => {
    let name = commit.author_name;
    if (name !== null) {
      commit_map[name] = commit_map[name] ? commit_map[name] + 1 : 1;
      total_commits += 1;
    }
  });

  // add issues to the member map
  var total_issues = 0;
  let issue_map = new Map();
  issueInfo.forEach((issue: any) => {
    let assignee = issue.assignee;
    total_issues += 1;
    if (assignee !== null) {
      issue_map[assignee.username] = issue_map[assignee.username] ? issue_map[assignee.username] + 1 : 1;
    }
  });

  const aboutCards = memberInfo.map((member) => {
    let commits = 0;
    if(commit_map[member.name] != null) {
      commits += commit_map[member.name]
    }
    if(commit_map[member.username] != null) {
      commits += commit_map[member.username]
    }
    return (
      <AboutCard name={member.name} image={member.photo} bio={member.bio} responsibilities={member.responsibilities} commits={commits} issues={issue_map[member.username]} unit_tests={member.unittests} />
    )
  });

  const apiCards = apiInfo.map((member) => {
    return (
      <AboutToolCard title={member.title} link={member.url} description={member.description} />
    )
  });

  return (
    <div className="about">
      <div className="background overflow-hidden" style={{ zIndex: -1, position: "relative"}}>
        <ParticlesContainer /> 
      </div>
      <main className="scroll-container">
        <Row>
          <section>
            <div className='text-center text-white mb-5'>
              <div style={{backgroundColor:'black'}}>
                <div className="bg"> Description </div>
                
                <h5 className="reg-text">Clear Skies is a website that aggregates data and visuals about the celestial bodies in our solar system. A lot of information about space can be highly technical, making it difficult for people to understand.Our goal is to provide information about planets, moons, launches, and satellites in a clear, easily accessible fashion, so that anybody who is interested in space can learn more, regardless of knowledge level.</h5>

                <h6 className="small-text"><a href="https://documenter.getpostman.com/view/23611574/2s83tGoBya">Postman Documentation</a> | <a href="https://gitlab.com/risamaida2114/cs373-idb/-/tree/main">GitLab Repository</a></h6> 

                <div style={{height: '120px'}}></div>
              </div>

            </div>
          </section>
        </Row>

        <Row>
          <section>
            <div style={{ backgroundColor:'white'}} className='p-5 text-dark text-center mb-5'>
              <h2 className='mb-3'>Team Members</h2>
              <h6 className='mb-3'>Total Commits: {total_commits}, Total Issues: {total_issues}</h6>
              <AboutGrid tiles={aboutCards} />
            </div>
          </section>
        </Row>

        <Row>
          <section>
            <div className='text-center text-white mb-5'>
              <div style={{backgroundColor:'black'}}>
                <div className="bg"> Integrating Disparate Data. </div>

                <h5 className="reg-text">One interesting result of integrating the data was seeing how the quantity of planets and moons relate to each other. Because planets are so much bigger than moons, most of the focus is on them when learning about space. However, there are significantly more moons than planets! Furthermore, moons are something we tend to associate as revolving around planets. Interestingly, most moons are actually orbiting asteroids!</h5>

                <div style={{height: '120px'}}></div>
              </div>
            </div>
          </section>
        </Row>

        <Row>
            <section>
              <div style={{ backgroundColor:'white'}} className='p-5 text-dark text-center mb-5'>
                <h1 className='mt-5 mb-5'>API Links</h1>
                <div className='mb-5'>
                  <AboutGrid tiles={apiCards} />
                </div>
                <div style={{height: '50px'}}></div>
              </div>
            </section>
        </Row>
      
        <Row>
          <section>
            <div className='p-5 text-center text-white'>
              <h1 className='mb-3'>Tools Used</h1>
              <ListGroup className='mb-5' style={{paddingLeft: '15rem', paddingRight: '15rem'}}>
                <ListGroup.Item variant="dark" className='mb-4'><h4>Amazon RDS</h4><h6>Amazon RDS is an open-source cloud database service that we used to host our database.</h6></ListGroup.Item>

                <ListGroup.Item variant="dark" className='mb-4'><h4>AWS Amplify</h4><h6>AWS Amplify is hosting our website. It also provides the SSL/TLS certificate required for HTTPS protocol.</h6></ListGroup.Item>

                <ListGroup.Item variant="dark" className='mb-4'><h4>Black</h4><h6>Black is a python code formatter that we used to standardize the format of our back-end code.</h6></ListGroup.Item>

                <ListGroup.Item variant="dark" className='mb-4'><h4>Discord</h4><h6>Discord is an instant messaging platform that we used for scheduling meetings, planning projects, and relaying project updates.</h6></ListGroup.Item>

                <ListGroup.Item variant="dark" className='mb-4'><h4>Flask and Flask SQLAlchemy</h4><h6>Flask provides a back-end web framework for Python. Flask SQL Alchemy is an extension of Flask used specifically for database queries and interaction. We used these in combination to populate our database.</h6></ListGroup.Item>

                <ListGroup.Item variant="dark" className='mb-4'><h4>GitLab</h4><h6>GitLab is an online Git repository that we used as a development environment. GitLab was used to plan and delegate the project through GitLab issues, and hosted the code used by AWS Amplify.</h6></ListGroup.Item>

                <ListGroup.Item variant="dark" className='mb-4'><h4>Jest</h4><h6>Jest is a JavaScript testing framework that we used to test our front-end code.</h6></ListGroup.Item>

                <ListGroup.Item variant="dark" className='mb-4'><h4>NameCheap</h4><h6>Name Cheap was used to acquire a free domain name.</h6></ListGroup.Item>

                <ListGroup.Item variant="dark" className='mb-4'><h4>PostgreSQL</h4><h6>PostgreSQL is an open-source relational database system that we used to populate our database.</h6></ListGroup.Item>

                <ListGroup.Item variant="dark" className='mb-4'><h4>Postman</h4><h6>Postman is an API platform used to build the documentation for our API.</h6></ListGroup.Item>

                <ListGroup.Item variant="dark" className='mb-4'><h4>React</h4><h6>React was used to build our website’s user interface. Every page on the site was designed using React.</h6></ListGroup.Item>

                <ListGroup.Item variant="dark" className='mb-4'><h4>React Bootstrap</h4><h6>React Bootstrap integrates Bootstrap’s JavaScript components into ones that are purely React. This was used to design the layout of our web pages.</h6></ListGroup.Item>

                <ListGroup.Item variant="dark" className='mb-4'><h4>Selenium</h4><h6>Selenium is an open-source automation testing tool that we used for our front-end GUI acceptance tests.</h6></ListGroup.Item>

                <ListGroup.Item variant="dark" className='mb-4'><h4>TypeScript</h4><h6>TypeScript is a strongly typed programming language that acts as an extension on JavaScript. TypeScript was used to design the framework for our front-end design.</h6></ListGroup.Item>
              </ListGroup>
            </div>
          </section>
        </Row>
      </main>
    </div>
  );
}

export default About
