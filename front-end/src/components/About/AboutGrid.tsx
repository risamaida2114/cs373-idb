import React from 'react'
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

export default function Grid ({
    tiles 
} : any ) {
    return (
    <Row xs={2} md={4} className="g-4">
        {Array.from({ length: tiles.length }).map((_, idx) => (
        <Col key={idx}>
            {tiles[idx]}
        </Col>
        ))}
    </Row>
    );
}