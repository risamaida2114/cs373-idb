import Card from 'react-bootstrap/Card';
import { useState } from 'react';
import './AboutToolCard.css';

export type Tool = {
    title ?: string;
    link ?: string; 
    description ?: string;
};

export default function AboutToolCard({
    title = "",
    link = "",
    description = "",
}: Tool) {

  const [flip, setFlip] = useState(false);
  return (
    <Card
        key={'dark'}
        text={'white'}
        className="text-center card-front"
        style={{ width: '20rem', height: '30rem'}}
    >
        <div className="pt-5">
            <a href={link}><Card.Title className='pt-5'>{title}</Card.Title></a>
        </div>
        <Card.Body>{description}</Card.Body>
    </Card> 
  );
}
