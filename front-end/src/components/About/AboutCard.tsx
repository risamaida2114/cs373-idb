import React from 'react';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';

export default function AboutCard({
  name = "",
  image = "",
  bio = "",
  responsibilities = "",
  commits = 0,
  issues = 0,
  unit_tests = 0
}) {
  return (
    <Card bg={'dark'} key={'dark'} text={'white'} className="">
        <Card.Img variant="top" src={image}  />
        <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Text>{bio}</Card.Text>
        </Card.Body>

        <ListGroup className="list-group-flush">
            <ListGroup.Item>{responsibilities}</ListGroup.Item>
            <ListGroup.Item>{commits} commits, {issues} issues, {unit_tests} unit tests</ListGroup.Item>
        </ListGroup>
    </Card>
  );
}
