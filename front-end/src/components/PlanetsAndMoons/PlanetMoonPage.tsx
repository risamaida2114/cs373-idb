import ParticlesContainer from '../ParticlesContainer';
import React, { useEffect, useState } from 'react';
import PlanetMoonPageCard from './PlanetMoonPageCard';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import '../About/about.css';
import { Container, Row, Col, Card } from 'react-bootstrap';
import ConnectionCard from '../ConnectionCard';
import ConnectionsGrid from '../ConnectionsGrid';

export type Body = {
  id?: number;
  name?: string;
  mass?: number;
  density?: number;
  gravity?: number;
  mean_radius?: number;
  discovery_date?: string;
  body_type?: string;
  image?: string;
  discoveredBy?: string;
  avgTemp?: number;
  orbit_diff_1?: number;
  orbit_diff_2?: number;
  orbit_diff_3?: number;
  satellite_1?: number;
  satellite_2?: number;
  satellite_3?: number;
  satellite_orbit_1?: number;
  satellite_orbit_2?: number;
  satellite_orbit_3?: number;
};

function PlanetPage() {
  const {bodyId} = useParams();
  const api_url = "https://api.clear-skies.me"
  const [bodyData, setBodyData] = useState<Body>();
  const [name, setName] = useState("");
  // const [connections, setConnections] = useState<Connections>();
  const[error, setError] = useState(null);

  useEffect(() => {
    axios(api_url + '/planetsmoons/' + bodyId)
    .then((response) => {
      setBodyData(response.data.planetmoon);
      setName(response.data.planetmoon.name);
      setError(null)
    })
      .catch(error);
  }, [bodyId]);

  const connectionCards = ["1", "2", "3"].map((num) => {
    // if body data is undefined
    if (!bodyData) {
      return; 
    }

    const id = bodyData["satellite_" + num];
    const so_diff = bodyData["satellite_orbit_" + num];
    return (
      <ConnectionCard {...{id, so_diff}}/>
    )
  });  

  return (
    <div className='about'>
      <div className="background overflow-hidden" style={{ zIndex : -1, position : "relative"}}> 
        <ParticlesContainer />
      </div>
        <main className="scroll-container">
        <Container fluid style={{height: '100%'}}>
          <Row>
            <section>
              <div className='mt-5 text-center text-white'>
                <h1 className='' style={{fontSize: '4rem'}}>{name}</h1>
              </div>
            </section>
            <section>
              <div className=''>
                <PlanetMoonPageCard {...bodyData}/>
              </div>
            </section>
  
          </Row>
        </Container>
        <Container fluid style={{backgroundColor:'white', height: '100%'}}>
          <Row>
            <section>
              <div className='p-5 text-dark text-center mb-5'>
                <h1 className='mb-5'>Find Satellites by Orbital Period</h1>
                <ConnectionsGrid tiles={connectionCards} />
              </div>
            </section>
            <Card></Card>
          </Row>
        </Container>

        </main>
    </div>
  )
}

export default PlanetPage;