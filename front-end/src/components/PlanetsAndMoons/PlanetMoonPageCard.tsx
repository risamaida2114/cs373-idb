import Card from 'react-bootstrap/Card';
import ReactCardFlip from 'react-card-flip';
import { useState } from 'react';
import '../PageCard.css'
import Repeat from '../../assets/repeat.png';
import moon_def from '../../images/moon_def.jpg';

export type Body = {
  id?: number;
  name?: string;
  mass?: number;
  density?: number;
  gravity?: number;
  mean_radius?: number;
  discovery_date?: string;
  body_type?: string;
  image?: string;
  discoveredBy?: string;
  avgTemp?: number;
  orbit_diff_1?: number;
  orbit_diff_2?: number;
  orbit_diff_3?: number;
  satellite_1?: number;
  satellite_2?: number;
  satellite_3?: number;
  satellite_orbit_1?: number;
  satellite_orbit_2?: number;
  satellite_orbit_3?: number;
};

export default function PlanetMoonPageCard({
  id = 0,
  name = "",
  mass = 0.0,
  density = 0.0,
  gravity = 0.0,
  mean_radius = 0.0,
  discovery_date = "",
  body_type = "",
  image = "",
  discoveredBy = "",
  avgTemp = 0.0,
  orbit_diff_1 = 0.0,
  satellite_orbit_1 = 0.0,
}: Body) {

  const [flip, setFlip] = useState(false);
  const checkImage = image === "./images/moon_def.jpg";
  return (
      <ReactCardFlip isFlipped={flip} 
          flipDirection="horizontal">
        <Card
          key={'dark'}
          text={'white'}
          className="text-center card-front"
          style={{ width: '25rem', cursor: 'pointer'}}
          onClick={() => setFlip(!flip)}
        >
          <Card.Img variant='top' src={checkImage?moon_def:image}/>
            <Card.Footer className="footer-front container"> 
              Learn more. <img src={Repeat} width={25} height={25} alt="plus icon" className="plus-icon"/>
            </Card.Footer>
        </Card>
  
        <Card
          key={'dark'}
          text={'white'}
          className="text-left card-front"
          style={{ width: '25rem', cursor: 'pointer'}}
          onClick={() => setFlip(!flip)}
        >
          <Card.Body>
            <Card.Title style={{fontWeight: 'bold'}}>Additional Information</Card.Title>
                <p><b>Mass: </b> {Number(mass).toExponential(2)} kg</p>
                <p><b>Gravity: </b> {Number(gravity).toFixed(2)} ms<sup>-2</sup></p>
                <p><b>Body Type: </b> {body_type} </p> 
                <p><b>Density: </b> {density} kg/m<sup>3</sup></p>
                <p><b>Mean Radius: </b> {Number(mean_radius).toFixed(2)} km</p>
                <p><b>Discovery Date: </b> {discovery_date}</p>
                <p><b>Average Temperature: </b> {Number(avgTemp).toFixed(2)} K</p>
                <p><b>Discovered By: </b> {discoveredBy}</p>
                <p><b>Orbital Period: </b> {orbit_diff_1 + satellite_orbit_1}</p>
            </Card.Body>
            <Card.Footer className="footer-front container"> 
              <img src={Repeat} width={25} height={25} alt="plus icon" className="plus-icon"/>
            </Card.Footer>
        </Card>
              
      </ReactCardFlip>
  );
}
