import React from 'react'
import { useState, useEffect } from 'react'
import axios from 'axios'
import PlanetAndMoonImageCard from './PlanetMoonImageCard'
import { Link } from 'react-router-dom';
import PaginateCards from '../PaginateCards'
import SearchBar from '../SearchBar';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import RangeSlider from '../RangeSlider';
import DropDownSelect from '../Select';
import '../ModelPage.css'
import '../top-bar.css'

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

function PlanetsAndMoons() {
  const [bodies, setBodies] = useState([]);
  const [metadata, setMetadata] = useState([] as any);
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [error, setError] = useState(null);
  const [sort, setSort] = useState<string>("");
  const sortableAttributes = ["Name", "Mass", "Density", "Gravity", "Mean Radius"]
  const [filters, setFilters] = useState<string>("");
  const [open, setOpen] = React.useState(false);
  const [pageIndex, setPageIndex] = useState(1);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  useEffect(() => {
    let search = searchTerm ? `&search=${searchTerm}` : "";
    let req = `https://api.clear-skies.me/planetsmoons?page=${pageIndex}${sort}${search}${filters}`;

    axios(`${req}`)
      .then((response) => {
        setBodies(response.data.planetsmoons);
        setMetadata(response.data.metadata);
        setError(null);
      })
      .catch(setError);
    }, [pageIndex, searchTerm, sort, filters]);
  if (error) return <p>An error occurred in PlanetsAndMoons.tsx</p>

  const cards = bodies.map(({ id, name, mass, density, gravity, mean_radius, discovery_date, image }) => {
    return (
      <Link to={"/planets-and-moons/"+id}>
        <PlanetAndMoonImageCard 
            name = {name}
            mass = {mass}
            density = {density}
            gravity = {gravity}
            mean_radius = {mean_radius} 
            discovery_date = {discovery_date}
            image = {image}
            search = {searchTerm}
        />
      </Link>
    )
  })

  return (
    <div>
      <div className="top-bar" style={{display : "flex", flexWrap: "wrap-reverse", justifyContent: "flex-end", marginRight: "5rem"}}>
        <div className="wrapper">
          <button onClick={handleOpen}>Filters</button>
              <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
              >
              <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">Select Filters</Typography>

                <RangeSlider
                  displayTerm={"Mass Exponent"}
                  reqTerm={"mass"}
                  filterVals={[0, 27]}
                  req={filters}
                  setReq={setFilters}
                />

                <RangeSlider
                  displayTerm={"Density"}
                  reqTerm={"density"}
                  filterVals={[0, 150]}
                  req={filters}
                  setReq={setFilters}
                />

                <RangeSlider
                  displayTerm={"Gravity"}
                  reqTerm={"gravity"}
                  filterVals={[0, 25]}
                  req={filters}
                  setReq={setFilters}
                />

                <RangeSlider
                  displayTerm={"Mean Radius"}
                  reqTerm={"radius"}
                  filterVals={[0, 70]}
                  req={filters}
                  setReq={setFilters}
                />

                <RangeSlider
                  displayTerm={"Average Temperature"}
                  reqTerm={"temp"}
                  filterVals={[0, 150]}
                  req={filters}
                  setReq={setFilters}
                />

                <button onClick={handleClose} style={{float: "right"}}>Apply</button>
              </Box>
            </Modal>
        </div>


        <div className="sort" style={{paddingTop: "2.1rem", paddingRight : "1rem"}}>
            <DropDownSelect
              displayTerm="Sort By"
              reqTerm="sort"
              categories={sortableAttributes}
              req={sort}
              setReq={setSort}
            />
        </div>

        <SearchBar 
          placeholder="Search Planets and Moons"
          searchTerm={searchTerm}
          setSearchTerm={setSearchTerm}
        />
      </div>
      <PaginateCards tiles={cards} total_tiles={metadata["total_planetsmoons"] ?? 0 } pageParam={pageIndex} setPageParam={setPageIndex}/>
    </div>
  );
}

export default PlanetsAndMoons
