import React from 'react'
import Card from 'react-bootstrap/Card';
import 'react-slideshow-image/dist/styles.css'
import '../Card.css'
import moon_def from '../../images/moon_def.jpg';
import Highlighter from "react-highlight-words";

export default function PlanetAndMoonImageCard({
  name = "",
  mass = "",
  density = "",
  gravity = "",
  mean_radius = "",
  discovery_date = "",
  image = "",
  search = ""
  
}) {
  // const navigate = useNavigate();

  // const handleClick = (type: string, name : string) => {
  //   navigate('/' + type.toLowerCase() + '/' + name.toLowerCase());
  // }
  const checkImage = image === "./images/moon_def.jpg";
  return (
      <Card
      bg={'dark'}
      key={'dark'}
      text={'white'}
    >
      <Card.Img variant="top" src={checkImage?moon_def:image} />
      <Card.Body>
          <Card.Title>
            <Highlighter
              highlightClassName="highlighter"
              searchWords={search.split(" ") ?? []}
              autoEscape={true}
              textToHighlight= {name}
            />
          </Card.Title>
          <Card.Text>
            <p className="p3"><b>Mass: </b> {Number(mass).toExponential(2)} kg</p>
            <p className="p3"><b>Gravity: </b> {Number(gravity).toFixed(2)} ms<sup>-2</sup></p>
            <p className="p3"><b>Density: </b> {density} kg/m<sup>3</sup></p>
            <p className="p3"><b>Discovery Date: </b> 
              <Highlighter
                highlightClassName="highlighter"
                searchWords={search.split(" ") ?? []}
                autoEscape={true}
                textToHighlight= {discovery_date}
              /></p>
            <p className="p3"><b>Mean Radius: </b> {Number(mean_radius).toFixed(2)} km</p>
          </Card.Text>
      </Card.Body>
    </Card>
  );
}
