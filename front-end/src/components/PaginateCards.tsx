import Pagination from '@mui/material/Pagination';

import React from 'react';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { Container } from 'react-bootstrap'
import Grid from './Grid';

function PaginateCards({tiles, total_tiles, pageParam, setPageParam}) {
    const TILES_PER_PAGE = 12;

    const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setPageParam(value.toString());
    }

    let pageIndex = parseInt(pageParam ?? "1", 10);
    let start = (pageIndex - 1) * TILES_PER_PAGE;
    let end = start + tiles.length;

    let totalPages = Math.ceil(total_tiles / TILES_PER_PAGE);
    if(Number.isNaN(totalPages)) {
        totalPages = 0;
    }

    return (
        <Container>
            <Row className="gridTiles">
                <Grid tiles={tiles} />
            </Row>
            <Row className="pagination">
                <Col>
                    <Pagination
                        count={totalPages}
                        defaultPage={1}
                        page={pageIndex}
                        showFirstButton={true}
                        showLastButton={true}
                        boundaryCount={0}
                        siblingCount={1}
                        onChange={handleChange}
                    />
                </Col>
                <Col>
                    <p><i>Showing ({start + 1} - {end}) out of {total_tiles}</i></p>
                </Col>
            </Row>
        </Container>
    )
}

export default PaginateCards;
