import React from 'react'
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import './Card.css'
// import ModuleCard from './Card';

// type GridProps = {
//     tiles : typeof ModuleCard[]
// }

export default function Grid ({
    tiles 
} : any ) {
    return (
        <Container className="g-5">
            <Row xs={2} md={4} className="g-4">
                {Array.from({ length: tiles.length }).map((_, idx) => (
                <Col key={idx}>
                    {tiles[idx]}
                </Col>
                ))}
            </Row>
        </Container>
    );
}