import React from 'react'
import Particles from "react-particles";
import type { Engine } from "tsparticles-engine";
import { loadStarsPreset } from "tsparticles-preset-stars";

export default class ParticlesContainer extends React.Component {
  async customInit(engine: Engine): Promise<void> {
    await loadStarsPreset(engine);
  }

  render() {
    const options = {
      preset: "stars",
    };

    return <Particles options={options} init={this.customInit}/>;
  }
}
