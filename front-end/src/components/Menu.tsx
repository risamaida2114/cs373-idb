import React, { useState } from "react";
import DropDown from "./DropDown";
import "./Menu.css"

/* adapted from https://codesandbox.io/s/jt4dh?file=/src/styles.css:0-1403 */

function Menu({
    attributes,
    sortTerm,
    setSortTerm
  }: {
    attributes: string[]
    sortTerm: string
    setSortTerm: (value: string) => void
}) {
  const [showDropDown, setShowDropDown] = useState<boolean>(false);
  const [selectAttribute, setSelectAttribute] = useState<string>("");

  const toggleDropDown = () => {
    setShowDropDown(!showDropDown);
  };

  const dismissHandler = (event: React.FocusEvent<HTMLButtonElement>): void => {
    if (event.currentTarget === event.target) {
      setShowDropDown(false);
    }
  };

  const sortSelection = (sort: string): void => {
    setSelectAttribute(sort);
    setSortTerm(sort);
  };

  return (
    <>
    <div className="wrapper">
      <button
        className={showDropDown ? "active" : undefined}
        onClick={(): void => toggleDropDown()}
        onBlur={(e: React.FocusEvent<HTMLButtonElement>): void =>
          dismissHandler(e)
        }
      >
        <div>{selectAttribute ? "Sort By: " + selectAttribute : "Sort By"} </div>
        {showDropDown && (
          <DropDown
            attributes={attributes}
            showDropDown={false}
            toggleDropDown={(): void => toggleDropDown()}
            sortSelection={sortSelection}
          />
        )}
      </button>
    </div>
    </>
  );
}

export default Menu;