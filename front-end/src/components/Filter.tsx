import React, { useState } from "react";
import FilterDropDown from "./FilterDropDown";

export default function Filter({
    filterTerm,
    filterVals,
    req,
    setReq
  }: {
    filterTerm: string
    filterVals: number[]
    req: string
    setReq: (value: string) => void
}) {
  const [showDropDown, setShowDropDown] = useState<boolean>(false);
  const [selectAttribute, setSelectAttribute] = useState<string>("");

  // happens on click
  const toggleDropDown = () => {
    setShowDropDown(!showDropDown);
  };

  // happens on blur (ex: when something else is clicked)
  // do not change drop down status on blur
  const dismissHandler = (event: React.FocusEvent<HTMLButtonElement>): void => {
    if (event.currentTarget === event.target) {
      setShowDropDown(showDropDown);
    }
  };

  // const filterSelection = (sort: string): void => {
  //   setFilterTerm(sort);
  // };

  return (
    <>
    <div className="wrapper">
      <button
        className={showDropDown ? "active" : undefined}
        onClick={(): void => toggleDropDown()}
        onBlur={(e: React.FocusEvent<HTMLButtonElement>): void =>
          dismissHandler(e)
        }
      >
        <div>{filterTerm} </div>
          {showDropDown && (
              <FilterDropDown
                showFilterDropDown={showDropDown}
                toggleFilterDropDown={(): void => toggleDropDown()}
                filterTerm={filterTerm}
                filterVals={filterVals}
                req={req}
                setReq={setReq}
              />
          )}
      </button>
    </div>
    </>
  );
}