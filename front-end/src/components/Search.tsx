import React from 'react'
import { useState, useEffect } from 'react'
import axios from 'axios'
import PlanetAndMoonImageCard from './PlanetsAndMoons/PlanetMoonImageCard'
import SatelliteImageCard from './Satellites/SatelliteImageCard';
import LaunchImageCard from './Launches/LaunchImageCard';
import { Link } from 'react-router-dom';
import Row from 'react-bootstrap/Row';
import { Container } from 'react-bootstrap'
import PaginateCards from './PaginateCards'
import SearchBar from './SearchBar';
import Typography from '@mui/material/Typography';
import './PlanetsAndMoons/planets-and-moons.css'
import './top-bar.css'

export default function Search() {
  const [bodies, setBodies] = useState([]);
  const [satellites, setSatellites] = useState([]);
  const [launches, setLaunches] = useState([]);
  const [metadata, setMetadata] = useState([] as any);
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [error, setError] = useState(null);
  const [plmPageIndex, setPlmPageIndex] = useState(1);
  const [lncPageIndex, setLncPageIndex] = useState(1);
  const [satPageIndex, setSatPageIndex] = useState(1);
  
  useEffect(() => {
    let req = `https://api.clear-skies.me/search?search=${searchTerm}&lncpage=${lncPageIndex}&satpage=${satPageIndex}&plmpage=${plmPageIndex}`;

    axios(`${req}`)
      .then((response) => {
        setBodies(response.data.planetsmoons);
        setSatellites(response.data.satellites);
        setLaunches(response.data.launches);
        setMetadata(response.data.metadata);
        setError(null);
      })
      .catch(setError);
    }, [searchTerm, lncPageIndex, satPageIndex, plmPageIndex]);
  if (error) return <p>An error occurred in PlanetsAndMoons.tsx</p>

  const bodies_cards = bodies.map(({ id, name, mass, density, gravity, mean_radius, discovery_date, body_type, image }) => {
    return (
      <Link to={"/planets-and-moons/"+id}>
        <PlanetAndMoonImageCard 
            name = {name}
            mass = {mass}
            density = {density}
            mean_radius = {mean_radius} 
            gravity = {gravity}
            discovery_date = {discovery_date}
            image = {image}
            search = {searchTerm}
        />
      </Link>
    )
  })

  const launch_cards = launches.map(({date_utc, flight_number, name, reused, rocket_image, ships, static_fire_date, success }) => {
    return (
      <Link to={"/launches/" + flight_number}>
        <LaunchImageCard
          date_utc = {date_utc}
          name = {name}
          reused = {reused}
          rocket_image = {rocket_image}
          ships = {ships}
          static_fire_date = {static_fire_date}
          success = {success} 
          search = {searchTerm}
          />
      </Link>
    )
  })

  const satellite_cards = satellites.map(({ name, number, size, type,
    launch_date, country, orbital_period, image }) => {
      return (
        <Link to={"/satellites/" + number}>
        <SatelliteImageCard 
          name={name}
          size={size}
          type={type}
          launch_date={launch_date}
          country={country}
          orbital_period={orbital_period}
          image={image}
          search = {searchTerm}
        />
        </Link>
      )
    });

  return (
    <div>
      <div className="top-bar" style={{display : "flex", flexWrap: "wrap-reverse", justifyContent: "flex-end", marginRight: "5rem"}}>
        <SearchBar 
          placeholder="Search All"
          searchTerm={searchTerm}
          setSearchTerm={setSearchTerm}
        />
      </div>
        <Container style={{display: "flex", justifyContent: "center"}}>
            <Typography className="modelTitle" variant="h4" align="center"> Planets and Moons </Typography>
            <Row><PaginateCards tiles={bodies_cards} total_tiles={metadata["total_planetsmoons"] ?? 0}
                  pageParam={plmPageIndex} setPageParam={setPlmPageIndex}/></Row>
            <Typography className="modelTitle" variant="h4" align="center"> Launches </Typography>
            <Row><PaginateCards tiles={launch_cards} total_tiles={metadata["total_launches"] ?? 0}
                  pageParam={lncPageIndex} setPageParam={setLncPageIndex}/></Row>
            <Typography className="modelTitle" variant="h4" align="center"> Satellites </Typography>
            <Row><PaginateCards tiles={satellite_cards} total_tiles={metadata["total_satellites"] ?? 0}
                  pageParam={satPageIndex} setPageParam={setSatPageIndex}/></Row>
        </Container>
    </div>
  );
}
