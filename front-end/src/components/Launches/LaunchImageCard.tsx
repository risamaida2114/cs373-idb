import React from 'react'
import Card from 'react-bootstrap/Card';
import 'react-slideshow-image/dist/styles.css'
import '../Card.css'
import Highlighter from "react-highlight-words";

export default function LaunchImageCard({
    date_utc = "",
    name = "",
    reused = "",
    rocket_image = "",
    ships = "",
    static_fire_date = "",
    success = "",
    search = "",
}) {
  const capitalize = (type: string) => {
    if(type != null && type.length > 0) {
      return type.charAt(0).toUpperCase() + type.slice(1);
    }
    return type;
  }

  return (
    <Card
    bg={'dark'}
    key={'dark'}
    text={'white'}
    className="custom-card"
    >
    <Card.Img variant="top" src={rocket_image} />
    <Card.Body>
        <Card.Title>
          <Highlighter
            highlightClassName="highlighter"
            searchWords={search.split(" ") ?? []}
            autoEscape={true}
            textToHighlight= {name}
          />
        </Card.Title>
        <Card.Text>
          <p className="p3"><b>Ships: </b> 
            <Highlighter
              highlightClassName="highlighter"
              searchWords={search.split(" ") ?? []}
              autoEscape={true}
              textToHighlight= {ships}
            /></p>
          <p className="p3"><b>Success: </b> {capitalize(success)} </p>
          <p className="p3"><b>Cores Reused: </b> {capitalize(reused)} </p>
          <p className="p3"><b>Launch Date: </b> 
            <Highlighter
              highlightClassName="highlighter"
              searchWords={search.split(" ") ?? []}
              autoEscape={true}
              textToHighlight= {date_utc}
            /> </p>
          <p className="p3"><b>Static Fire Date: </b> 
            <Highlighter
              highlightClassName="highlighter"
              searchWords={search.split(" ") ?? []}
              autoEscape={true}
              textToHighlight= {static_fire_date}
              /></p>
        </Card.Text>
    </Card.Body>
  </Card>
  );
}
