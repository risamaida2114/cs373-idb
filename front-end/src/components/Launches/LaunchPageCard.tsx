import Card from 'react-bootstrap/Card';
import ReactCardFlip from 'react-card-flip';
import { useState } from 'react';
import '../PageCard.css'
import Repeat from '../../assets/repeat.png';
import YoutubeEmbed from './YoutubeEmbed';

export type Launch = {
  article ?: string;
  cores_success ?: string;
  date_utc ?: string;
  details ?: string;
  flight_number ?: number;
  name ?: string;
  patch_image ?: string;
  reddit_thread ?: string;
  reused ?: string;
  rocket_image ?: string;
  ships ?: string;
  static_fire_date ?: string;
  success ?: string;
  wikipedia ?: string;
  youtube_id ?: string;
};

export default function LaunchPageCard({
  article = "",
  cores_success = "",
  date_utc = "",
  details = "",
  flight_number = 0,
  name = "",
  patch_image = "",
  reddit_thread = "",
  reused = "",
  rocket_image = "",
  ships = "",
  static_fire_date = "",
  success = "",
  wikipedia = "",
  youtube_id = "",
}: Launch) {

  const [flip, setFlip] = useState(false);
  return (
      <ReactCardFlip isFlipped={flip} 
          flipDirection="horizontal">
        <Card
          key={'dark'}
          text={'white'}
          className="text-center card-front"
          style={{ width: '50rem', cursor: 'pointer'}}
          onClick={() => setFlip(!flip)}
        >
          <YoutubeEmbed embedId={youtube_id} />
            <Card.Footer className="footer-front container"> 
              Learn more. <img src={Repeat} width={25} height={25} alt="plus icon" className="plus-icon"/>
            </Card.Footer>
        </Card>
  
        <Card
          key={'dark'}
          text={'white'}
          className="text-left card-front"
          style={{ width: '50rem', cursor: 'pointer'}}
          onClick={() => setFlip(!flip)}
        >
          <Card.Body>
            <Card.Title style={{fontWeight: 'bold'}}>Additional Information</Card.Title>
                <p><b>Ships:</b> {ships}</p>
                <p><b>Cores Reused:</b> {reused}</p>
                <p><b>Cores Success:</b> {cores_success}</p>
                <p><b>Launch Name:</b> {name}</p>
                <p><b>Launch Success:</b> {success}</p>
                <p><b>Launch Date:</b> {date_utc}</p>
                <p><b>Static Fire Date:</b> {static_fire_date}</p>
                <p><b>Details:</b> {details}</p>
                <p><b>Links: </b> <a href={reddit_thread} target="_blank" rel="noreferrer">Reddit Thread</a> | <a href={wikipedia} target="_blank" rel="noreferrer">Wikipedia</a> | <a href={article} target="_blank" rel="noreferrer">Article</a></p>
            </Card.Body>
            <Card.Footer className="footer-front container"> 
              <img src={Repeat} width={25} height={25} alt="plus icon" className="plus-icon"/>
            </Card.Footer>
        </Card>
              
      </ReactCardFlip>
  );
}
