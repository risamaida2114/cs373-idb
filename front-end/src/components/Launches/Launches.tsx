import React from 'react'
import { useState, useEffect } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom';
import Row from 'react-bootstrap/Row';
import { Container } from 'react-bootstrap'
import PaginateCards from '../PaginateCards'
import LaunchImageCard from './LaunchImageCard';
import SearchBar from '../SearchBar';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import RangeSlider from '../RangeSlider';
import Box from '@mui/material/Box';
import DropDownSelect from '../Select';
import '../ModelPage.css'
import '../top-bar.css'

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

function Launches() {
  const [launches, setLaunches] = useState([] as any);
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [sort, setSort] = useState<string>("");
  const [metadata, setMetadata] = useState([] as any);
  const [error, setError] = useState(null);
  const sortableAttributes = ["Name", "Date UTC",  "Success", "Ships", "Static Fire Date"]
  const [filters, setFilters] = useState<string>("");
  const [open, setOpen] = React.useState(false);
  const [pageIndex, setPageIndex] = useState(1);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  useEffect(() => {
    let search = searchTerm ? `&search=${searchTerm}` : "";
    let req = `https://api.clear-skies.me/launches?page=${pageIndex}${sort}${search}${filters}`;

    axios(`${req}`)
    .then((response) => {
      setLaunches(response.data.launches);
      setMetadata(response.data.metadata);
      setError(null);
    })
    .catch(setError)
  }, [pageIndex, searchTerm, sort, filters]);
  if (error) return <p>An error occurred in Launches.tsx</p>

  const cards = launches.map(({date_utc, flight_number, name, reused, rocket_image, ships, static_fire_date, success }) => {
    return (
      <Link to={"/launches/" + flight_number}>
        <LaunchImageCard
          date_utc = {date_utc}
          name = {name}
          reused = {reused}
          rocket_image = {rocket_image}
          ships = {ships.toString()}
          static_fire_date = {static_fire_date}
          success = {success} 
          search = {searchTerm}/>
      </Link>
    )
  })

  return (
    <div>
      <div className="top-bar" style={{display : "flex", flexWrap: "wrap-reverse", justifyContent: "flex-end", marginRight: "5rem"}}>
        <div className="wrapper">
            <button onClick={handleOpen}>Filters</button>
                <Modal
                  open={open}
                  onClose={handleClose}
                  aria-labelledby="modal-modal-title"
                  aria-describedby="modal-modal-description"
                >
                <Box sx={style}>
                  <Typography id="modal-modal-title" variant="h6" component="h2">Select Filters</Typography>

                  <DropDownSelect
                    displayTerm="Reused"
                    reqTerm="reused"
                    categories={["True", "False"]}
                    req={filters}
                    setReq={setFilters}
                  />

                  <DropDownSelect
                    displayTerm="Success"
                    reqTerm="success"
                    categories={["True", "False"]}
                    req={filters}
                    setReq={setFilters}
                  />

                  <DropDownSelect
                    displayTerm="Ships"
                    reqTerm="ships"
                    categories={["0", "1", "2", "3", "4", "5"]}
                    req={filters}
                    setReq={setFilters}
                  />

                  <RangeSlider
                    displayTerm={"Static Fire Date"}
                    reqTerm={"sdate"}
                    filterVals={[2006, 2022]}
                    req={filters}
                    setReq={setFilters}
                  />

                  <RangeSlider
                    displayTerm={"Launch Date"}
                    reqTerm={"date"}
                    filterVals={[2006, 2022]}
                    req={filters}
                    setReq={setFilters}
                  />

                  <button onClick={handleClose} style={{float: "right"}}>Apply</button>
                </Box>
              </Modal>
          </div>

          <div className="sort" style={{paddingTop: "2.1rem", paddingRight : "1rem"}}>
            <DropDownSelect
              displayTerm="Sort By"
              reqTerm="sort"
              categories={sortableAttributes}
              req={sort}
              setReq={setSort}
            />
          </div>

        <SearchBar 
          placeholder="Search Launches"
          searchTerm={searchTerm}
          setSearchTerm={setSearchTerm}
        />
      </div>
      <Container>
        <Row><PaginateCards tiles={cards} total_tiles={metadata["total_launches"] ?? 0 } pageParam={pageIndex} setPageParam={setPageIndex}/></Row>
      </Container>
    </div>
  );
}

export default Launches
