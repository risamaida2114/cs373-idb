import ParticlesContainer from '../ParticlesContainer';
import React, { useEffect, useState } from 'react';
import PageCard from './LaunchPageCard';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import '../About/about.css';
import { Container, Row, Col, Card } from 'react-bootstrap';
import ConnectionCard from '../ConnectionCard';
import ConnectionsGrid from '../ConnectionsGrid';

export type Launch = {
  article ?: string;
  cores_success ?: string;
  date_utc ?: string;
  details ?: string;
  launchId ?: number;
  name ?: string;
  patch_image ?: string;
  reddit_thread ?: string;
  reused ?: string;
  rocket_image ?: string;
  ships ?: string;
  static_fire_date ?: string;
  success ?: string;
  wikipedia ?: string;
  youtube_id ?: string;
  satellite_1 ?: string;
  satellite_2 ?: string;
  satellite_3 ?: string;
};

function LaunchPage() {
  const {launchId} = useParams();
  const api_url = "https://api.clear-skies.me"
  const [launchData, setLaunchData] = useState<Launch>();
  const [name, setName] = useState("");
  const[error, setError] = useState(null);

  useEffect(() => {
    axios(api_url + '/launches/' + launchId)
    .then((response) => {
      setLaunchData(response.data.launch);
      setName(response.data.launch.name);
      setError(null);
    })
      .catch(error);
  }, [launchId]);

  const connectionCards = ["1", "2", "3"].map((num) => {
    if (!launchData) {
      return; 
    }

    const id = launchData["satellite_" + num];
    return (
      <ConnectionCard {...{id}}/>
    )
  });  

  return (
    <div className='about'>
      <div className="background overflow-hidden" style={{ zIndex : -1, position : "relative"}}> 
        <ParticlesContainer />
      </div>
        <main className="scroll-container">
        <Container fluid style={{height: '100%'}}>
          <Row>
            <section>
              <div className='mt-5 text-center text-white'>
                <h1 className='' style={{fontSize: '4rem'}}>{name}</h1>
              </div>
            </section>
            <section>
              <div className=''>
                <PageCard {...launchData}/>
              </div>
            </section>
  
          </Row>
        </Container>
        <Container fluid style={{backgroundColor:'white', height: '100%'}}>
          <Row>
            <section>
              <div className='p-5 text-dark text-center mb-5'>
                <h1 className='mb-5'>Find Satellites Launched On Similar Date</h1>
                <ConnectionsGrid tiles={connectionCards} />
              </div>
            </section>
            <Card></Card>
          </Row>
        </Container>

        </main>
    </div>
  )
}

export default LaunchPage;