import * as React from 'react';
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';
import './Slider.css'

export default function VerticalSlider({
    filterVals,
    req,
    setReq
  }: {
    filterVals: number[]
    req: string
    setReq: (value: string) => void
}) {
    const [value, setValue] = React.useState<number[]>([filterVals[0], filterVals[1]]);

    function preventHorizontalKeyboardNavigation(event: React.KeyboardEvent) {
      if (event.key === 'ArrowLeft' || event.key === 'ArrowRight') {
        event.preventDefault();
      }
    }

    function valuetext(value: number) {
        return `${value}`;
    }

    const handleChange = (event: Event, newValue: number | number[]) => {
        setValue(newValue as number[]);
    };

    const defaultVal = filterVals[0] + (filterVals[1] - filterVals[0]) / 2;
  
    return (
        <div className="wrapper">
            <Box sx={{ height: 100 }}>
                <Slider
                sx={{
                    '& input[type="range"]': {
                    WebkitAppearance: 'slider-vertical',
                    },
                    color : 'white',
                }}
                value={value}
                orientation="vertical"
                defaultValue={defaultVal}
                onChange={handleChange}
                valueLabelDisplay="auto"
                getAriaValueText={valuetext}
                onKeyDown={preventHorizontalKeyboardNavigation}
                />
            </Box>
        </div>
    );
  }
