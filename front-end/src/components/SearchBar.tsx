import React from "react";
import "./SearchBar.css";
import SearchIcon from "@material-ui/icons/Search";

export default function SearchBar({
  placeholder,
  searchTerm,
  setSearchTerm
}: {
  placeholder: string
  searchTerm: string
  setSearchTerm: (value: string) => void
}) {
  const handleFilter = (event) => {
    const searchWord = event.target.value;
    setSearchTerm(searchWord);
  };

  return (
      <div className="search">
        <div className="box">
          <input
            type="text"
            placeholder={placeholder}
            value={searchTerm}
            onChange={handleFilter}
          />
          <div className="searchIcon">
            <SearchIcon />
          </div>
        </div>
      </div>
  );
}


