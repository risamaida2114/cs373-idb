import React, { useEffect, useState } from 'react'
import axios from 'axios';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ConnectionCard(
  satelliteInfo : any,
) {
  const api_url = "https://api.clear-skies.me"
  const [name, setName] = useState("");
  const [image, setImage] = useState("");
  const[error, setError] = useState(null);

  useEffect(() => {
    axios(api_url + '/satellites/' + satelliteInfo["id"])
    .then((response) => {
      setName(response.data.satellite.name);
      setImage(response.data.satellite.image);
      setError(null);
    })
      .catch((error));
  }, [satelliteInfo["id"]]);

  return (
    <Link to={"/satellites/"+satelliteInfo["id"]}>
      <Card bg={'dark'} key={'dark'} text={'white'} className="">
          <Card.Img variant="top" src={image} />
          <Card.Footer style={{fontSize: '1.5rem'}}>{name}</Card.Footer>
      </Card>
    </Link>
  );
}
