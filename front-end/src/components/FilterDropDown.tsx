import React, { useEffect, useState } from 'react';
import RangeSlider from './Slider';
// import 'rc-slider/assets/index.css';

/* adapted from https://codesandbox.io/s/jt4dh?file=/src/styles.css:0-1403 */

type FilterDropDownProps = {
  showFilterDropDown: boolean;
  toggleFilterDropDown: Function;
  filterTerm: string;
  filterVals: number[];
  req: string;
  setReq: (value: string) => void;
};

const FilterDropDown: React.FC<FilterDropDownProps> = ({
  filterTerm,
  filterVals,
  req,
  setReq,
}: FilterDropDownProps): JSX.Element => {
  const [showFilterDropDown, setShowFilterDropDown] = useState<boolean>(false);
  
  useEffect(() => {
    setShowFilterDropDown(showFilterDropDown);
  }, [showFilterDropDown]);

  return (
    <>
      <div>
        <RangeSlider
          filterVals={filterVals}
          req={req}
          setReq={setReq}
        />
        {/* <>
          <Range />
        </> */}
      </div>
    </>
  );
};

export default FilterDropDown;
