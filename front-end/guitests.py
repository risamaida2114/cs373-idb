# based on: https://nander.cc/using-selenium-within-a-docker-container
import unittest

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait

URL = "https://dev.clear-skies.me/"


class GUITests(unittest.TestCase):
    def create_driver(self):
        chrome_options = Options()
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_prefs = {}
        chrome_options.experimental_options["prefs"] = chrome_prefs
        # Disable images
        chrome_prefs["profile.default_content_settings"] = {"images": 2}
        driver = webdriver.Chrome(options=chrome_options)
        driver.get(URL)
        return driver


    def test_title(self):
        print("checks website title")
        driver = self.create_driver()
        assert driver.title == "Clear Skies"
        driver.quit()

    def test_nav_bar(self):
        print("checks existence of buttons on navigation bar")

        driver = self.create_driver()
        navbar = driver.find_elements(By.CLASS_NAME, "nav-item")

        assert len(navbar) == 8

        pages = ["Home", "Search", "Planets and Moons", "Launches", "Satellites", "Visualizations", "Provider Visualizations", "About Us"]
        for page in navbar:
            assert page.text in pages

        driver.quit()

    def test_nav_bar_links(self):
        print("checks links on navigation bar")

        driver = self.create_driver()
        navbar = driver.find_elements(By.CLASS_NAME, "nav-link")

        for page in navbar:
            link = page.text.replace(' ', '-').lower()
            page.click()

            if link == "home":
                assert driver.current_url == URL
            elif link == "about-us":
                assert driver.current_url == URL + "about"
            else:
                assert driver.current_url == URL + link

        driver.quit()

    def test_navbar_from_other_pages(self) :
        print("checks that the navbar is visible on every main page")

        driver = self.create_driver()
        navbar = driver.find_elements(By.CLASS_NAME, "nav-link")

        for page in navbar:
            page.click()

            new_navbar = driver.find_elements(By.CLASS_NAME, "nav-link")
            assert len(new_navbar) == 8

        driver.quit()
    
    def test_planets_and_moons_cards(self) :
        print("checks that at least one card exists on the Planets and Moons page")

        driver = self.create_driver()
        driver.get(URL + "planets-and-moons")

        cards = WebDriverWait(driver, timeout=5).until(lambda d: d.find_elements(By.CSS_SELECTOR, "div.card-body"))
        assert len(cards) > 0

        driver.quit()

    def test_satellites(self) :
        print("checks that at least one card exists on the Satellite page")

        driver = self.create_driver()
        driver.get(URL + "satellites")

        cards = WebDriverWait(driver, timeout=5).until(lambda d: d.find_elements(By.CSS_SELECTOR, "div.card-body"))
        assert len(cards) > 0

        driver.quit()

    def test_launches(self) :
        print("checks that at least one card exists on the Launches page")

        driver = self.create_driver()
        driver.get(URL + "launches")

        cards = WebDriverWait(driver, timeout=5).until(lambda d: d.find_elements(By.CSS_SELECTOR, "div.card-body"))
        assert len(cards) > 0

        driver.quit()

    def test_planets_and_moons_instance(self) :
        print("checks that Planet and Moon cards link to instance pages")

        driver = self.create_driver()
        driver.get(URL + "planets-and-moons")

        cards = WebDriverWait(driver, timeout=5).until(lambda d: d.find_elements(By.CSS_SELECTOR, "#root > div:nth-child(2) > div.container > div.gridTiles.row > div > div > div:nth-child(1) > a"))
        links = [card.get_attribute('href') for card in cards]
        assert len(cards) > 0

        old_url = driver.current_url
        cards[0].click()

        assert driver.current_url != old_url
        assert driver.current_url == links[0]

        driver.quit()

    def test_satellite_instance(self) :
        print("checks that Satellite cards link to instance pages")

        driver = self.create_driver()
        driver.get(URL + "satellites")

        cards = WebDriverWait(driver, timeout=5).until(lambda d: d.find_elements(By.CSS_SELECTOR, "#root > div:nth-child(2) > div.container > div > div > div:nth-child(1) > div > div > div:nth-child(1) > a"))
        links = [card.get_attribute('href') for card in cards]
        assert len(cards) > 0

        old_url = driver.current_url
        cards[0].click()

        assert driver.current_url != old_url
        assert driver.current_url == links[0]

        driver.quit()

    def test_launches_instance(self) :
        print("checks that Launch cards link to instance pages")

        driver = self.create_driver()
        driver.get(URL + "launches")

        cards = WebDriverWait(driver, timeout=5).until(lambda d: d.find_elements(By.CSS_SELECTOR, "#root > div:nth-child(2) > div.container > div > div > div:nth-child(1) > div > div > div:nth-child(1) > a"))
        links = [card.get_attribute('href') for card in cards]
        assert len(cards) > 0

        old_url = driver.current_url
        cards[0].click()

        assert driver.current_url != old_url
        assert driver.current_url == links[0]

        driver.quit()

if __name__ == '__main__':
    unittest.main()
